<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY docbook.dsl PUBLIC "-//Norman Walsh//DOCUMENT DocBook HTML Stylesheet//EN" CDATA DSSSL>
]>

<style-sheet>

<style-specification use="docbook">
<style-specification-body> 

;; pathname to a header and footer file that will be included.

(define %stylesheet% "\ampersand;StyleSheet;")

(element type ($bold-mono-seq$))

;; let the user override the CLASS with an explicit ROLE attribute.

(element function
  (let* ((role (attribute-string "role"))
	 (class (if role role "function")))
    (make element gi: "TT"
          attributes: (list
                       (list "CLASS" (normalize class)))
          (make element gi: "B"
                (process-children)))))

;; this is necessary because right now jadetex does not understand
;; symbolic entities, whereas things work well with numeric entities.
(declare-characteristic preserve-sdata?
          "UNREGISTERED::James Clark//Characteristic::preserve-sdata?"
          #f)

;; put the legal notice in a separate file
(define %generate-legalnotice-link%
  #t)

;; use graphics in admonitions, and have their path be "stylesheet-images"
;; NO: they do not yet look very good
(define %admon-graphics-path%
  "./stylesheet-images/")
(define %admon-graphics%
  #f)

;; make funcsynopsis look pretty
(define %funcsynopsis-decoration%
  ;; Decorate elements of a FuncSynopsis?
  #t)

(define %html-ext% ".html")
(define %root-filename% "index")
(define %body-attr%
  ;; What attributes should be hung off of BODY?
;;  '())
  (list
   (list "BGCOLOR" "#FFFFFF")
   (list "TEXT" "#000000")))

(define %generate-article-toc% 
  ;; Should a Table of Contents be produced for Articles?
  ;; If true, a Table of Contents will be generated for each 'Article'.
  #t)

(define %generate-part-toc% #t)

(define %shade-verbatim%
  #t)

(define %use-id-as-filename%
  ;; Use ID attributes as name for component HTML files?
  #t)

(define %graphic-default-extension% "gif")


;; I want to change this so that for programlisting we use a blue background,
;; but for example or informalexample we use green.
(define %shade-verbatim-attr-2% 
  ;; Attributes used to create a shaded verbatim environment.
  (lambda ()
    (if (or (equal? (gi (parent (current-node)))
		    (normalize "example"))
	    (equal? (gi (parent (current-node)))
		    (normalize "informalexample")))
	(list
	 (list "BORDER" "0")
	 (list "BGCOLOR" "#E8F8E8")
	 (list "WIDTH" "100%")
	 (list "CELLPADDING" "6"))
	(list
	 (list "BORDER" "0")
	 (list "BGCOLOR" "#E8E8F8")
	 (list "WIDTH" "100%")
	 (list "CELLPADDING" "6"))
	)
  )
)

;; This overrides $verbatim-display$ (copied from 1.20, dbverb.dsl).
;; It simply changes %shade-verbatim-attr% to (%shade-verbatim-attr-2%) so we
;; can use our own function to set the different verbatim colours.
(define ($verbatim-display$ line-numbers?)
  (let ((content (make element gi: "PRE"
		       attributes: (list
				    (list "CLASS" (gi)))
		       (if line-numbers?
			   ($verbatim-content-with-linenumbers$)
			   ($verbatim-content$)))))
    (if %shade-verbatim%
	(make sequence
		(para-check)
		(make element gi: "TABLE"
		      attributes: (%shade-verbatim-attr-2%)
		      (make element gi: "TR"
			    (make element gi: "TD"
				  content)))
		(para-check 'restart))
	content)))


;; This overrides the tgroup definition (copied from 1.20, dbtable.dsl).
;; It changes the table background color, cell spacing and cell padding.
(element tgroup
  (let* ((wrapper   (parent (current-node)))
	 (frameattr (attribute-string (normalize "frame") wrapper))
	 (pgwide    (attribute-string (normalize "pgwide") wrapper))
	 (footnotes (select-elements (descendants (current-node)) 
				     (normalize "footnote")))
	 (border (if (equal? frameattr (normalize "none"))
		     '(("BORDER" "0"))
		     '(("BORDER" "1"))))
	 (bgcolor '(("BGCOLOR" "#FFE0E0")))
	 (width (if (equal? pgwide "1")
		    (list (list "WIDTH" ($table-width$)))
		    '()))
	 (head (select-elements (children (current-node)) (normalize "thead")))
	 (body (select-elements (children (current-node)) (normalize "tbody")))
	 (feet (select-elements (children (current-node)) (normalize "tfoot"))))
    (make element gi: "TABLE"
	  attributes: (append
		       border
		       width
		       bgcolor
		       '(("CELLSPACING" "0"))
		       '(("CELLPADDING" "4"))
		       (if %cals-table-class%
			   (list (list "CLASS" %cals-table-class%))
			   '()))
	  (process-node-list head)
	  (process-node-list body)
	  (process-node-list feet)
	  (make-table-endnotes))))

;; This overrides the refsect2 definition (copied from 1.20, dbrfntry.dsl).
;; It puts a horizontal rule after each function/struct/... description.
(element refsect2
  (make sequence
    (make empty-element gi: "HR")
    ($block-container$)))

;; Override the book declaration, so that we generate a crossreference
;; for the book

(element book 
  (let* ((bookinfo  (select-elements (children (current-node)) (normalize "bookinfo")))
	 (ititle   (select-elements (children bookinfo) (normalize "title")))
	 (title    (if (node-list-empty? ititle)
		       (select-elements (children (current-node)) (normalize "title"))
		       (node-list-first ititle)))
	 (nl       (titlepage-info-elements (current-node) bookinfo))
	 (tsosofo  (with-mode head-title-mode
		     (process-node-list title)))
	 (dedication (select-elements (children (current-node)) (normalize "dedication"))))
    (make sequence
     (html-document 
      tsosofo
      (make element gi: "DIV"
	    attributes: '(("CLASS" "BOOK"))
	    (if %generate-book-titlepage%
		(make sequence
		  (book-titlepage nl 'recto)
		  (book-titlepage nl 'verso))
		(empty-sosofo))
	    
	    (if (node-list-empty? dedication)
		(empty-sosofo)
		(with-mode dedication-page-mode
		  (process-node-list dedication)))
	    
	    (if (not (generate-toc-in-front))
		(process-children)
		(empty-sosofo))
	    
	    (if %generate-book-toc%
		(build-toc (current-node) (toc-depth (current-node)))
		(empty-sosofo))
	    
	    ;;	  (let loop ((gilist %generate-book-lot-list%))
	    ;;	    (if (null? gilist)
	    ;;		(empty-sosofo)
	    ;;		(if (not (node-list-empty? 
	    ;;			  (select-elements (descendants (current-node))
	    ;;					   (car gilist))))
	    ;;		    (make sequence
	    ;;		      (build-lot (current-node) (car gilist))
	    ;;		      (loop (cdr gilist)))
	    ;;		    (loop (cdr gilist)))))
	  
	    (if (generate-toc-in-front)
		(process-children)
		(empty-sosofo))))
     (make entity 
       system-id: ".index-temp.sgml"
       (with-mode generate-index-mode
	 (process-children))))))

;; Mode for generating cross references

(define (process-child-elements)
  (process-node-list
   (node-list-map (lambda (snl)
                    (if (equal? (node-property 'class-name snl) 'element)
                        snl
                        (empty-node-list)))
                  (children (current-node)))))

(mode generate-index-mode
  (element anchor
    (if (attribute-string "href" (current-node))
	(empty-sosofo)
	(make formatting-instruction data:
	      (string-append "\less-than-sign;ANCHOR id =\""
			     (attribute-string "id" (current-node))
			     "\" href=\""
			     (href-to (current-node))
			     "\"\greater-than-sign;
"))))

  ;; We also want to be able to link to complete RefEntry.
  (element refentry
    (make sequence
      (make formatting-instruction data:
	    (string-append "\less-than-sign;ANCHOR id =\""
			   (attribute-string "id" (current-node))
			   "\" href=\""
			   (href-to (current-node))
			   "\"\greater-than-sign;
"))
      (process-child-elements)))

  (default
    (process-child-elements)))

;; For hypertext links for which no target is found in the document, we output
;; our own special tag which we use later to resolve cross-document links.
(element link 
  (let* ((target (element-with-id (attribute-string (normalize "linkend")))))
    (if (node-list-empty? target)
      (make element gi: "GTKDOCLINK"
	    attributes: (list
			 (list "HREF" (attribute-string (normalize "linkend"))))
            (process-children))
      (make element gi: "A"
            attributes: (list
                         (list "HREF" (href-to target)))
            (process-children)))))

(define ($html-body-start$)
  (make entity-ref name: "html-body-start"))

(define ($html-body-end$)
  (make entity-ref name: "html-body-end"))

(define %html-pubid% "-//GNOME Documentation//DTD DocBook HTML 1.0//EN")

(define (html-entity-file htmlfilename)
  ;; Returns the filename that should be used for _writing_ htmlfilename.
  ;; We need to replace the .html suffix with .sgml here.
  (let ((outputdir (dbhtml-value (sgml-root-element) "output-dir"))
	(htmlfilebase (substring htmlfilename 0
		       (- (string-length htmlfilename)
		           (string-length %html-ext%)))))
    (if (and use-output-dir outputdir)
        (string-append outputdir "/" htmlfilebase ".sgml")
        (string-append htmlfilebase ".sgml"))))

;; This is required to produce valid HTML 4.0
(define %fix-para-wrappers% #t)
(define %spacing-paras% #f)

;; Override this here to produce valid HTML 4.0
(define ($informal-object$)
  (let ((id (attribute-string (normalize "id"))))
    (make sequence
	  (para-check)
	  (make element gi: "DIV"
		attributes: (list
			     (list "CLASS" (gi)))
		(if id
		    (make element gi: "A"
			  attributes: (list (list "NAME" id))
			  (empty-sosofo))
		    (empty-sosofo))

		(if %spacing-paras%
		    (make element gi: "P" (empty-sosofo))
		    (empty-sosofo))
		
		(process-children)
		
		(if %spacing-paras%
		    (make element gi: "P" (empty-sosofo))
		    (empty-sosofo)))
	  (para-check 'restart))))
  
</style-specification-body>
</style-specification>

<external-specification id="docbook" document="docbook.dsl">

</style-sheet>
