#!/bin/sh

xmllint --noout --valid style-guide.xml
mkdir html
cd html
rm *.html
rm *.xml

xsltproc ../gdsg-stylesheet.xsl ../style-guide.xml

# Strip out all the NBSPs that get printed as odd chars
# in some browsers...
for file in *.html ; do
	 sed -e '/\xA0/ s/\xA0//g' $file > tmp
	 mv tmp $file
done
