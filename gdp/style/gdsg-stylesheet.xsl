<?xml version="1.0" encoding="utf-8"?>
<!-- Customization layer -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">


<!-- Use 'chunk.xsl' in line below to chunk files. -->
<xsl:import href="http://docbook.sourceforge.net/release/xsl/current/html/chunk.xsl"/>

<xsl:param name="show.comments">1</xsl:param>

<xsl:param name="toc.section.depth">2</xsl:param>

<xsl:param name="generate.toc">
book	toc
chapter toc
</xsl:param>



<xsl:param name="chunk.section.depth" select="1"/>
<xsl:param name="use.id.as.filename" select="'1'"/>
<xsl:param name="generate.legalnotice.link" select="1"/>
<xsl:param name="admon.graphics" select="1"/>

</xsl:stylesheet>
