<?xml version="1.0"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                  "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" []>

<sect1 id="indexingsect">
  <title>Indexing</title>
  <para>The information in this section is organized as follows:
    <itemizedlist>
      <listitem><para><link linkend="indexchapter">Creating an Index Chapter</link></para></listitem>
      <listitem><para><link linkend="indexentries">Creating Index Entries</link></para></listitem>
    </itemizedlist>
  </para>
  <sect2 id="indexchapter">
  <title>Creating an Index Chapter</title>
  <para>Use the block <filename>&lt;index&gt;</filename> element to create an Index chapter in your book.</para>
  <para>Insert this element before the closing <filename>&lt;/book&gt;</filename> element, as shown in the following example:
    <screen>
      .
      .
      .
      &lt;index&gt;&lt;indexterm&gt;&lt;primaryie&gt;&lt;/primaryie&gt;&lt;/indexterm&gt;&lt;/index&gt;
      &lt;/book&gt;
    </screen>
  </para>
  </sect2>
  <sect2 id="indexentries">
  <title>Creating Index Entries</title>
  <para>Use the inline <filename>&lt;indexterm&gt;</filename> element to create index entries.</para>
  <para>The syntax of the <filename>&lt;indexterm&gt;</filename> element is as follows:
    <screen>
      &lt;indexterm [ id="<replaceable>id</replaceable>" | startref="<replaceable>id</replaceable>" ] [ class="<replaceable>class</replaceable>" ] &gt;
        &lt;primary&gt;<replaceable>primaryterm</replaceable>&lt;/primary&gt;
          [ &lt;secondary&gt;<replaceable>secondaryterm</replaceable>&lt;/secondary&gt; ]
            [ &lt;tertiary&gt;<replaceable>tertiaryterm</replaceable>&lt;/tertiary&gt; ]
        [ &lt;see&gt;<replaceable>cross-reference</replaceable>&lt;/see&gt; ]
        [ &lt;seealso&gt;<replaceable>cross-reference</replaceable>&lt;/seealso&gt; ]
      &lt;/indexterm&gt;
    </screen>
  </para>
  <para>The first six examples in the remainder of this section are taken from the <citetitle>GNOME 2.0 Desktop User Guide</citetitle>. The remaining two examples are arbitrary examples.
  </para>
    <example>
      <title>Creating a simple index entry</title>
        <para>Add the following code to the relevant section:
          <screen>
            &lt;indexterm&gt;
              &lt;primary&gt;access keys&lt;/primary&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>The above code creates the following index entry:
          <literallayout>
          access keys, 22
          </literallayout>
        </para>
    </example>
    <example>
      <title>Creating an index entry with two secondary entries</title>
        <para>Add the following code to the relevant section:
          <screen>
            &lt;indexterm&gt;
              &lt;primary&gt;applications&lt;/primary&gt;
                &lt;secondary&gt;overview&lt;/secondary&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>Add the following code to the relevant section:
          <screen>
            &lt;indexterm&gt;
              &lt;primary&gt;applications&lt;/primary&gt;
                &lt;secondary&gt;help&lt;/secondary&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>The above code creates the following index entry:
          <literallayout>
          applications
            help, 41
            overview, 40
          </literallayout>
        </para>
    </example>
    <example>
      <title>Creating an index entry with two tertiary entries</title>
        <para>Add the following code to the relevant section:
          <screen>
            &lt;indexterm&gt;
              &lt;primary&gt;file manager&lt;/primary&gt;
                &lt;secondary&gt;emblems&lt;/secondary&gt;
                  &lt;tertiary&gt;introduction&lt;/tertiary&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>Add the following code to the relevant section:
          <screen>
            &lt;indexterm&gt;
              &lt;primary&gt;file manager&lt;/primary&gt;
                &lt;secondary&gt;emblems&lt;/secondary&gt;
                  &lt;tertiary&gt;adding&lt;/tertiary&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>The above code creates the following index entry:
          <literallayout>
          file manager
              .
              .
              .
              emblems
                  adding, 124
                  introduction, 106
          </literallayout>
        </para>
    </example>
    <example>
      <title>Creating a cross-reference as a simple index entry</title>
        <para>Add the following code to the relevant section:
          <screen>
            &lt;indexterm&gt;
              &lt;primary&gt;preference tools&lt;/primary&gt;
                &lt;secondary&gt;Keyboard Accessibility&lt;/secondary&gt;
            &lt;/indexterm&gt;
            &lt;indexterm&gt;
              &lt;primary&gt;AccessX&lt;/primary&gt;
                &lt;see&gt;preference tools, Keyboard Accessibility&lt;/see&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>The above code creates the following index entry:
          <literallayout>
          AccessX
             <emphasis>See</emphasis> preference tools, Keyboard Accessibility
          .
          .
          .
          preference tools
              .
              .
              .
              Keyboard Accessibility, 198
          </literallayout>
        </para>
    </example>
    <example>
      <title>Creating a cross-reference as a secondary index entry</title>
        <para>Add the following code to the relevant section:
          <screen>
            &lt;indexterm&gt;
              &lt;primary&gt;FTP sites, accessing&lt;/primary&gt;
            &lt;/indexterm&gt;
            &lt;indexterm&gt;
              &lt;primary&gt;file manager&lt;/primary&gt;
                &lt;secondary&gt;FTP sites&lt;/secondary&gt;
                  &lt;see&gt;FTP sites&lt;/see&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>The above code creates the following index entry:
          <literallayout>
          file manager
              .
              .
              .
              FTP sites
                 <emphasis>See</emphasis> FTP sites
              .
              .
              .
          FTP sites, accessing, 114
          </literallayout>
        </para>
    </example>
    <example>
      <title>Creating several index entries for the same text</title>
        <para>Add the following code to the relevant section:
          <screen>
            &lt;indexterm&gt;
              &lt;primary&gt;Trash&lt;/primary&gt;
                &lt;secondary&gt;displaying&lt;/secondary&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>Add the same code to the relevant section:
          <screen>
            &lt;indexterm&gt;
              &lt;primary&gt;Trash&lt;/primary&gt;
                &lt;secondary&gt;displaying&lt;/secondary&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>The above code creates the following index entry:
          <literallayout>
          Trash
            displaying, 122, 155
          </literallayout>
        </para>
    </example>
    <example>
      <title>Adding a cross-reference to an index entry</title>
        <para>Add the following code to the relevant section:
          <screen>
            &lt;indexterm&gt;
              &lt;primary&gt;view pane&lt;/primary&gt;
                &lt;secondary&gt;introduction&lt;/secondary&gt;
                &lt;seealso&gt;viewer components&lt;/seealso&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>The above code creates the following index entry:
          <literallayout>
          view pane
            introduction, 104
           <emphasis>See also</emphasis> viewer components
          </literallayout>
        </para>
    </example>
    <example>
      <title>Specifying a page range in an index entry</title>
        <para>Add the following code at the start of the relevant section:
          <screen>
            &lt;indexterm id="basics" class="startofrange"&gt;
              &lt;primary&gt;basic skills&lt;/primary&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>Add the following code at the end of the relevant section:
          <screen>
            &lt;indexterm startref="basics" class="endofrange"&gt;
            &lt;/indexterm&gt;
          </screen>
        </para>
        <para>The above code creates the following index entry:
          <literallayout>
          basic skills, 15-24
          </literallayout>
        </para>
    </example>
  </sect2>
</sect1>
