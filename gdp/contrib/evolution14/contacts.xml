<chapter id="contacts-1">
  <title>Using Contacts</title>
  <highlights>
    <para>The information in this chapter describes how to use <application>Evolution</application> to manage your contact information.</para>
  </highlights>
  <sect1 id="contacts-5">
    <title>Introduction to Contacts</title>
    <indexterm>
      <primary>address book</primary>
      <see>contacts</see>
    </indexterm>
    <indexterm>
      <primary>contacts</primary>
      <secondary>introduction to</secondary>
    </indexterm>
    <para><application>Evolution</application> enables you to store
information about your contacts in contacts folders. For each contact, you
can store the name, job title, telephone numbers, email addresses, and so
on. You can also create contact lists. You can use a contact list to send
a message to many of your contacts at one time.</para>
    <para><xref linkend="contacts-FIG-15"/> shows a typical contacts window.</para>
    <figure id="contacts-FIG-15">
      <title>Typical Contacts Window</title>
      <screenshot>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/contacts_window.png" format="PNG"/>
          </imageobject>
          <textobject>
            <phrase>Typical Contacts window.</phrase>
          </textobject>
        </mediaobject>
      </screenshot>
    </figure>
    <para><application>Evolution</application> uses the Vcard format for contact
information. </para>
    <para><application>Evolution</application> also enables you to use an LDAP
server for your contact information. For more information on how to configure
an LDAP server, see <xref linkend="gettingstarted-19"/>.</para>
  </sect1>
  <sect1 id="contacts-6">
    <title>Searching Your Contacts</title>
    <indexterm>
      <primary>contacts</primary>
      <secondary>searching</secondary>
    </indexterm>
    <para><application>Evolution</application> enables you to search your contacts.
You can use the searchbar to perform a simple search of all contacts in a
folder. For example, you can search for a message that contains a particular
text string. You can use the alphabetic index buttons to search for a contact
whose name begins with a particular letter.</para>
    <para>You can also perform advanced searches that use more complex search
criteria. For example, you can search for contacts by email address or category.</para>
    <sect2 id="contacts-19">
      <title>To Search All Contacts in a Folder</title>
      <para>To search all contacts in a folder, perform the following steps:</para>
      <orderedlist>
        <listitem>
          <para>Display the folder that contains the contacts that you want
to search.</para>
        </listitem>
        <listitem>
          <para>Select the contact component in which to search from the drop-down
list in the searchbar. </para>
        </listitem>
        <listitem>
          <para>Type the text that you want to search for in the text box
on the searchbar.</para>
        </listitem>
        <listitem>
          <para>Click on the <guibutton>Find Now</guibutton> button. Alternatively,
choose <menuchoice><guimenu>Search</guimenu><guimenuitem>Find Now</guimenuitem></menuchoice>. The contacts that match the search criteria are
displayed.</para>
          <para>To clear the search criteria and display all the contacts in the folder,
choose <menuchoice><guimenu>Search</guimenu><guimenuitem>Clear</guimenuitem></menuchoice>. Alternatively, click on the <guibutton>Clear</guibutton> button
in the searchbar.</para>
        </listitem>
      </orderedlist>
      <para>You can also use the alphabetic index buttons at the right side of the
window to search for contacts. To search for a contact whose name begins with
a particular letter, click on that letter. To search for contacts whose name
begins with a number or another non-alphabetic character, click on the <guibutton>123</guibutton> index button.</para>
    </sect2>
    <sect2 id="contacts-18">
      <title>To Perform an Advanced Search</title>
      <para>To perform an advanced search on your contacts, perform the following
steps:</para>
      <orderedlist>
        <listitem>
          <para>Display the folder that contains the contacts that you want
to search.</para>
        </listitem>
        <listitem>
          <para>Choose <menuchoice><guimenu>Tools</guimenu><guimenuitem>Search for Contacts</guimenuitem></menuchoice>. Alternatively, select <guilabel>Advanced</guilabel> from the drop-down list in the searchbar. An <guilabel>Advanced Search</guilabel> dialog is displayed.</para>
        </listitem>
        <listitem>
          <para>Use the <guilabel>If</guilabel> group box to create criteria
for the search. To create criteria, perform the following steps:</para>
          <orderedlist>
            <listitem>
              <para>From the first drop-down list, select the contact component
in which to search. For example, select <guilabel>Email</guilabel> to search
the text in the email address of the contact.</para>
            </listitem>
            <listitem>
              <para>From the second drop-down list, select the relationship between
the contact component and the search text. For example, to search for contacts
whose email address includes the search text, select <guilabel>contains</guilabel>
from the second drop-down list.</para>
            </listitem>
            <listitem>
              <para>In the text box, type the search text. This text is not case
sensitive.</para>
            </listitem>
            <listitem>
              <para>To add more criteria, click on the <guibutton>Add</guibutton>
button. To remove a criterion, click on the <guibutton>Remove</guibutton>
button beside the criterion.</para>
            </listitem>
          </orderedlist>
        </listitem>
        <listitem>
          <para>Select the appropriate option from the <guilabel>Execute actions</guilabel> drop-down list. Select one of the following options:</para>
          <itemizedlist>
            <listitem>
              <para><guilabel>if any criteria are met</guilabel>: Select this
option if you want the search to return matches where any of the specified
criteria are met.</para>
            </listitem>
            <listitem>
              <para><guilabel>if all criteria are met</guilabel>: Select this
option if you want the search to return only matches where all of the specified
criteria are met.</para>
            </listitem>
          </itemizedlist>
        </listitem>
        <listitem>
          <para>Click <guibutton>OK</guibutton> to perform the advanced search.
The contacts that match are displayed in the contacts folder.</para>
          <para>To clear the advanced search criteria and display all the messages in
the folder, choose <menuchoice><guimenu>Search</guimenu><guimenuitem>Clear</guimenuitem></menuchoice>.</para>
        </listitem>
      </orderedlist>
    </sect2>
  </sect1>
  <sect1 id="contacts-14">
    <title>Managing Your Contacts</title>
    <para>The following sections describe how to perform the following tasks:</para>
    <itemizedlist>
      <listitem>
        <para>Add contacts and contact lists.</para>
      </listitem>
      <listitem>
        <para>Edit contacts and contact lists.</para>
      </listitem>
      <listitem>
        <para>Delete contacts and contact lists.</para>
      </listitem>
      <listitem>
        <para>Save contacts and contact lists.</para>
      </listitem>
      <listitem>
        <para>Forward contacts and contact lists.</para>
      </listitem>
      <listitem>
        <para>Save contacts and contact lists.</para>
      </listitem>
      <listitem>
        <para>Move and copy contacts and contact lists.</para>
      </listitem>
      <listitem>
        <para>Send messages to contacts and contact lists.</para>
      </listitem>
      <listitem>
        <para>Select multiple contacts and contact lists.</para>
      </listitem>
    </itemizedlist>
    <sect2 id="contacts-7">
      <title>To Add a Contact</title>
      <indexterm>
        <primary>contacts</primary>
        <secondary>adding</secondary>
      </indexterm>
      <para>To add a contact, perform the following steps:</para>
      <orderedlist>
        <listitem>
          <para>Choose <menuchoice><guimenu>File</guimenu><guisubmenu>New</guisubmenu><guimenuitem>Contact</guimenuitem></menuchoice>. A <guilabel>Contact Editor</guilabel> window is displayed.</para>
        </listitem>
        <listitem>
          <para>Enter the contact details in the <guilabel>General</guilabel>
tabbed section. The following table describes the elements in the <guilabel>General</guilabel> tabbed section:</para>
          <informaltable frame="topbot">
            <tgroup cols="2" colsep="0" rowsep="0">
              <colspec colname="colspec0" colwidth="36.77*"/>
              <colspec colname="colspec1" colwidth="63.23*"/>
              <thead>
                <row rowsep="1">
                  <entry>
                    <para>Element</para>
                  </entry>
                  <entry>
                    <para>Description</para>
                  </entry>
                </row>
              </thead>
              <tbody>
                <row>
                  <entry valign="top">
                    <para>
                      <guibutton>Full Name</guibutton>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the full name of the contact. </para>
                    <para>Alternatively, click on the <guibutton>Full Name</guibutton> button. Use the <guilabel>Full Name</guilabel> dialog to create the full name of the contact. </para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Job title</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the job title of the contact.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Organization</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the organization to which the contact belongs.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>File as</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Use the drop-down list to select how to file the
contact in your list of contacts. The options in the drop-down list are generated
from the full name that you enter.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para><guilabel>Business</guilabel>, <guilabel>Home</guilabel>, <guilabel>Business fax</guilabel>, <guilabel>Mobile</guilabel></para>
                  </entry>
                  <entry valign="top">
                    <para>Use these elements to enter the telephone numbers
of the contact, and other information. </para>
                    <para>Select the category of
information that you want to enter from the category drop-down list to the
left of the text box. For example, to enter the business telephone number
of the contact, select <guilabel>Business</guilabel> from the drop-down list.
To enter the pager number of the contact, select <guilabel>Pager</guilabel>,
and so on.</para>
                    <para>Type the contact information for the category that
you selected in the text box.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Primary email</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the primary email address for the contact.</para>
                    <para>To enter another email address, select <guilabel>Email 2</guilabel>
or <guilabel>Email 3</guilabel> from the category drop-down list to the left
of the text box. Type the other email address for the contact in the text
box.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Wants to receive HTML mail</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Select this option if this contact wants to receive
email in HTML format. If you do not select this option, when you send email
in HTML format to this contact, <application>Evolution</application> converts
the email to plain text format.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Business</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Select the category of address that you want to
enter from the category drop-down list to the left of the address text area.
For example, to enter the home address of the contact, select <guilabel>Home</guilabel> from the drop-down list.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>Address field</para>
                  </entry>
                  <entry valign="top">
                    <para>Enter the address of the contact. </para>
                    <para>Alternatively, click on the <guibutton>Address</guibutton> button. Use the <guilabel>Full Address</guilabel> dialog
to create the full address of the contact. </para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>This is the mailing address</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Select this option to use the address in the address
field as the mail address for the contact.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Web page address</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Enter a URL for a web page for the contact in the
field. Click on the connect button to open the specified URL in the default
web browser.</para>
                  </entry>
                </row>
                <row>
                  <entry colname="colspec0" valign="top">
                    <para>
                      <guibutton>Categories</guibutton>
                    </para>
                  </entry>
                  <entry colname="colspec1" valign="top">
                    <para>Type the categories to which
the contact belongs in the text box. If you type more than one category, separate
the categories with a comma.</para>
                    <para>Alternatively, click on the <guibutton>Categories</guibutton> button. An <guilabel>Edit Categories</guilabel> dialog
is displayed. To select a category, click on the check box for the category
to display a check mark.</para>
                    <para>To add a new category, click on the <guibutton>Edit Master Category List</guibutton> button. An <guilabel>Edit Global Category
List</guilabel> dialog is displayed. Click on the text area at the top of
the category list, type the name of the new category, then press <keycap>Return</keycap>. To delete a category, select the category, then click on
the <guibutton>Remove</guibutton> button. Click <guibutton>Close</guibutton>
to close the <guilabel>Edit Global Category List</guilabel> dialog.</para>
                    <para>Click <guibutton>OK</guibutton> to put the information from the <guilabel>Edit Categories</guilabel> dialog into the text box in the <guilabel>General</guilabel> tabbed section, and close the dialog.</para>
                  </entry>
                </row>
              </tbody>
            </tgroup>
          </informaltable>
        </listitem>
        <listitem>
          <para>To enter more contact details, click on the <guilabel>Details</guilabel> tab. The following table describes the elements in the <guilabel>Details</guilabel> tabbed section:</para>
          <informaltable frame="topbot">
            <tgroup cols="2" colsep="0" rowsep="0">
              <colspec colwidth="30.70*"/>
              <colspec colwidth="69.30*"/>
              <thead>
                <row rowsep="1">
                  <entry>
                    <para>Element</para>
                  </entry>
                  <entry>
                    <para>Description</para>
                  </entry>
                </row>
              </thead>
              <tbody>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Department</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the department in which the contact works.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Office</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the office in which the contact works.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Profession</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the profession of the contact.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Manager's name</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the name of the manager of the contact.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Assistant's name</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the name of the assistant to the contact.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Nickname</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the nickname of the contact.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Spouse</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type the name of the spouse of the contact.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Birthday</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Use the drop-down combination box to specify the
date of the birthday of the contact.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Anniversary</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Use the drop-down combination box to specify the
date of the wedding anniversary of the contact.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Notes</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Type any other information that you want to add for the
contact.</para>
                  </entry>
                </row>
              </tbody>
            </tgroup>
          </informaltable>
        </listitem>
        <listitem>
          <para>To enter free or busy information, or other calendar information
for the contact, click on the <guilabel>Collaboration</guilabel> tab. The
following table describes the elements in the <guilabel>Collaboration</guilabel>
tabbed section:</para>
          <informaltable frame="topbot">
            <tgroup cols="2" colsep="0" rowsep="0">
              <colspec colwidth="34.14*"/>
              <colspec colwidth="65.86*"/>
              <thead>
                <row rowsep="1">
                  <entry>
                    <para>Element</para>
                  </entry>
                  <entry>
                    <para>Description</para>
                  </entry>
                </row>
              </thead>
              <tbody>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Public Calendar URL</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Enter a URL that you can access to view the public
calendar information for the contact. Click on the connect button to open
the specified URL in the default web browser.</para>
                  </entry>
                </row>
                <row>
                  <entry valign="top">
                    <para>
                      <guilabel>Free/Busy URL</guilabel>
                    </para>
                  </entry>
                  <entry valign="top">
                    <para>Enter a URL that you can access to view the free
or busy information for the contact. Click on the connect button to open the
specified URL in the default web browser.</para>
                  </entry>
                </row>
              </tbody>
            </tgroup>
          </informaltable>
        </listitem>
        <listitem>
          <para>To save the contact, choose <menuchoice><guimenu>File</guimenu><guimenuitem>Save</guimenuitem></menuchoice>.</para>
          <para>To save the contact and then close the <guilabel>Contact Editor</guilabel>
window, choose <menuchoice><guimenu>File</guimenu><guimenuitem>Save
and Close</guimenuitem></menuchoice>. </para>
          <para>To save the contact as a Vcard-format file, choose <menuchoice><guimenu>File</guimenu><guimenuitem>Save As</guimenuitem></menuchoice> to display
the <guilabel>Save Contact as VCard</guilabel> window. Type the filename,
then click <guibutton>OK</guibutton>.</para>
          <para>To close the <guilabel>Contact Editor</guilabel> window without saving
the contact, choose <menuchoice><guimenu>File</guimenu><guimenuitem>Close</guimenuitem></menuchoice>.</para>
        </listitem>
        <listitem>
          <para>To print the details of the contact, choose <menuchoice><guimenu>File</guimenu><guimenuitem>Print</guimenuitem></menuchoice>.</para>
        </listitem>
      </orderedlist>
      <note>
        <para>In <guilabel>Phone List</guilabel> and <guilabel>By Company</guilabel>
views, you can add a contact directly into the contacts window. To add a contact
in these views, click on the <guilabel>Click here to add a contact</guilabel>
area, then enter contact information in each of the available fields.</para>
      </note>
    </sect2>
    <sect2 id="contacts-8">
      <title>To Add a Contact List</title>
      <indexterm>
        <primary>contact lists</primary>
        <secondary>adding</secondary>
      </indexterm>
      <para>To add a contact list, perform the following steps:</para>
      <orderedlist>
        <listitem>
          <para>Choose <menuchoice><guimenu>File</guimenu><guisubmenu>New</guisubmenu><guimenuitem>Contact List</guimenuitem></menuchoice>. </para>
        </listitem>
        <listitem>
          <para>Type a name for the list in the <guilabel>List name</guilabel>
text box in the <guilabel>Contact List Editor</guilabel> window.</para>
        </listitem>
        <listitem>
          <para>To add an email address to the contact list, type the email
address in the text box at the top of the <guilabel>Members</guilabel> group
box, then click on the <guibutton>Add</guibutton> button. The email address
is added to the contact list.</para>
        </listitem>
        <listitem>
          <para>To add a contact from your <guilabel>Contacts</guilabel> folder
to the list, open your <guilabel>Contacts</guilabel> folder in a separate
window. Drag the contact from the <guilabel>Contacts</guilabel> folder to
the list in the lower part of the <guilabel>Members</guilabel> group box. </para>
        </listitem>
        <listitem>
          <para>To remove a member from the contact list, select the member
in the list in the lower part of the <guilabel>Members</guilabel> group box,
then click <guibutton>Remove</guibutton>.</para>
        </listitem>
        <listitem>
          <para>When you send a message to a contact list, the names or email
addresses of members of the list are added to the <guilabel>To</guilabel>
field of the message. If you do not want to send the names or email addresses
of members when you send messages to the list, select the <guilabel>Hide addresses
when sending mail to this list</guilabel> option. If you select this option,
when you send a message to the contact list, the names or email addresses
of members of the list are added to the <guilabel>Bcc</guilabel> field of
the message. </para>
        </listitem>
        <listitem>
          <para>To save the contact list, choose <menuchoice><guimenu>File</guimenu><guimenuitem>Save</guimenuitem></menuchoice>.</para>
          <para>To save the contact list and then close the <guilabel>Contact List Editor</guilabel> window, choose <menuchoice><guimenu>File</guimenu><guimenuitem>Save and Close</guimenuitem></menuchoice>. </para>
          <para>To save the contact list as a Vcard-format file, choose <menuchoice><guimenu>File</guimenu><guimenuitem>Save As</guimenuitem></menuchoice>
to display the <guilabel>Save List as VCard</guilabel> window. Type the filename,
then click <guibutton>OK</guibutton>.</para>
          <para>To close the <guilabel>Contact List Editor</guilabel> window without
saving the contact, choose <menuchoice><guimenu>File</guimenu><guimenuitem>Close</guimenuitem></menuchoice>.</para>
        </listitem>
      </orderedlist>
    </sect2>
    <sect2 id="contacts-16">
      <title>To Edit a Contact</title>
      <indexterm>
        <primary>contacts</primary>
        <secondary>editing</secondary>
      </indexterm>
      <para>To edit a contact, perform the following steps:</para>
      <orderedlist>
        <listitem>
          <para>In a contacts folder, select the contact that you want to
edit, then choose <menuchoice><guimenu>File</guimenu><guimenuitem>Open</guimenuitem></menuchoice>. Alternatively, double-click on the contact.</para>
          <para>A <guilabel>Contact Editor</guilabel> dialog is displayed with the details
of the contact.</para>
        </listitem>
        <listitem>
          <para>Use the <guilabel>Contact Editor</guilabel> dialog to modify
the information for the contact. For information on the elements in the <guilabel>Contact Editor</guilabel> dialog, see <xref linkend="contacts-7"/>.</para>
        </listitem>
        <listitem>
          <para>Choose <menuchoice><guimenu>File</guimenu><guimenuitem>Save</guimenuitem></menuchoice> to save your changes.</para>
        </listitem>
      </orderedlist>
    </sect2>
    <sect2 id="contacts-17">
      <title>To Edit a Contact List</title>
      <indexterm>
        <primary>contact lists</primary>
        <secondary>editing</secondary>
      </indexterm>
      <para>To edit a contact list, perform the following steps:</para>
      <orderedlist>
        <listitem>
          <para>In a contacts folder, select the contact list that you want
to edit, then choose <menuchoice><guimenu>File</guimenu><guimenuitem>Open</guimenuitem></menuchoice>. Alternatively, double-click on the contact
list.</para>
          <para>A <guilabel>Contact List Editor</guilabel> dialog is displayed with
the details of the contact list.</para>
        </listitem>
        <listitem>
          <para>Use the <guilabel>Contact List Editor</guilabel> dialog to
modify the information for the contact list. For information on the elements
in the <guilabel>Contact List Editor</guilabel> dialog, see <xref linkend="contacts-8"/>.</para>
        </listitem>
        <listitem>
          <para>Choose <menuchoice><guimenu>File</guimenu><guimenuitem>Save</guimenuitem></menuchoice> to save your changes.</para>
        </listitem>
      </orderedlist>
    </sect2>
    <sect2 id="contacts-10">
      <title>To Save a Contact or Contact List in Vcard Format</title>
      <indexterm>
        <primary>contacts</primary>
        <secondary>saving</secondary>
      </indexterm>
      <indexterm>
        <primary>contact lists</primary>
        <secondary>saving</secondary>
      </indexterm>
      <para>To save a contact or contact list as a file in Vcard format, perform
the following steps:</para>
      <orderedlist>
        <listitem>
          <para>In a contacts folder, select the contact or contact list that
you want to save, then choose <menuchoice><guimenu>File</guimenu><guimenuitem>Save as VCard</guimenuitem></menuchoice>.</para>
          <para>Alternatively, select the contact or contact list, then right-click
on the contact or contact list. Choose <guimenuitem>Save as VCard</guimenuitem>
from the popup menu.</para>
        </listitem>
        <listitem>
          <para>Use the <guilabel>Save as VCard</guilabel> dialog to specify
a location and name for the file.</para>
        </listitem>
      </orderedlist>
    </sect2>
    <sect2 id="contacts-11">
      <title>To Forward a Contact or Contact List</title>
      <indexterm>
        <primary>contacts</primary>
        <secondary>forwarding</secondary>
      </indexterm>
      <indexterm>
        <primary>contact lists</primary>
        <secondary>forwarding</secondary>
      </indexterm>
      <para>To send a contact or contact list as a file attachment in
Vcard format, perform the following steps:</para>
      <orderedlist>
        <listitem>
          <para>In a contacts folder, select the contact or contact list that
you want to send, then choose <menuchoice><guimenu>Actions</guimenu><guimenuitem>Forward Contact</guimenuitem></menuchoice>.</para>
          <para>Alternatively, select the contact or contact list, then right-click
on the contact or contact list. Choose <guimenuitem>Forward Contact</guimenuitem>
from the popup menu.</para>
          <para>A message window is displayed, with the contact or contact list added
as an attachment.</para>
        </listitem>
        <listitem>
          <para>Enter the email address to which you want to send the contact
or contact list in the <guilabel>To</guilabel> field.</para>
        </listitem>
        <listitem>
          <para>Type any text that you want to add to the message in the content
area, then choose <menuchoice><guimenu>File</guimenu><guimenuitem>Send</guimenuitem></menuchoice> from the message window.</para>
        </listitem>
      </orderedlist>
    </sect2>
    <sect2 id="contacts-22">
      <title>To Save a Contact or Contact List That You Receive</title>
      <indexterm>
        <primary>contacts</primary>
        <secondary>saving from message attachment</secondary>
      </indexterm>
      <indexterm>
        <primary>contact lists</primary>
        <secondary>saving from message attachment</secondary>
      </indexterm>
      <para>If you receive
a contact or contact list in a message as a file attachment in Vcard format,
you can save the contact or contact list to your contacts. </para>
      <para>In the message, click on the right arrow icon at the left side of the
button, then choose <guimenuitem>View Inline</guimenuitem>. The contact or
contact list is displayed in the message. Click on the <guibutton>Save to
addressbook</guibutton> button to add the contact or contact list.</para>
    </sect2>
    <sect2 id="contacts-12">
      <title>To Move a Contact or Contact List</title>
      <indexterm>
        <primary>contacts</primary>
        <secondary>moving</secondary>
      </indexterm>
      <indexterm>
        <primary>contact lists</primary>
        <secondary>moving</secondary>
      </indexterm>
      <para>To move a contact or contact list to another folder, perform the following
steps:</para>
      <orderedlist>
        <listitem>
          <para>In a contacts folder, select the contact or contact list that
you want to move, then choose <menuchoice><guimenu>Actions</guimenu><guimenuitem>Move to Folder</guimenuitem></menuchoice>.</para>
          <para>Alternatively, select the contact or contact list, then right-click
on the contact or contact list. Choose <guimenuitem>Move to folder</guimenuitem>
from the popup menu.</para>
        </listitem>
        <listitem>
          <para>Use the <guilabel>Move card to</guilabel> dialog to specify
the folder to which you want to move the contact or contact list.</para>
        </listitem>
      </orderedlist>
      <note>
        <para>You can only move a contact or contact list to folders whose folder
type is <guilabel>Contacts</guilabel>.</para>
      </note>
    </sect2>
    <sect2 id="contacts-13">
      <title>To Copy a Contact or Contact List</title>
      <indexterm>
        <primary>contacts</primary>
        <secondary>copying</secondary>
      </indexterm>
      <indexterm>
        <primary>contact lists</primary>
        <secondary>copying</secondary>
      </indexterm>
      <para>To copy a contact or contact list to another folder, perform
the following steps:</para>
      <orderedlist>
        <listitem>
          <para>In a contacts folder, select the contact or contact list that
you want to copy, then choose <menuchoice><guimenu>Actions</guimenu><guimenuitem>Copy to Folder</guimenuitem></menuchoice>.</para>
          <para>Alternatively, select the contact or contact list, then right-click
on the contact or contact list. Choose <guimenuitem>Copy to folder</guimenuitem>
from the popup menu.</para>
        </listitem>
        <listitem>
          <para>Use the <guilabel>Copy card to</guilabel> dialog to specify
the folder to which you want to copy the contact or contact list.</para>
        </listitem>
      </orderedlist>
      <note>
        <para>You can only copy a contact or contact list to folders whose folder
type is <guilabel>Contacts</guilabel>.</para>
      </note>
    </sect2>
    <sect2 id="contacts-9">
      <title>To Delete a Contact or Contact List</title>
      <indexterm>
        <primary>contacts</primary>
        <secondary>deleting</secondary>
      </indexterm>
      <indexterm>
        <primary>contact lists</primary>
        <secondary>deleting</secondary>
      </indexterm>
      <para>To delete a contact or contact list, select the contact
or contact list that you want to delete, then choose <menuchoice><guimenu>Edit</guimenu><guimenuitem>Delete</guimenuitem></menuchoice>. </para>
      <para>A confirmation dialog is displayed. Click <guibutton>Delete</guibutton>
to delete the contact or contact list.</para>
    </sect2>
    <sect2 id="contacts-21">
      <title>To Send a Message to a Contact or Contact List</title>
      <indexterm>
        <primary>contacts</primary>
        <secondary>sending message to</secondary>
      </indexterm>
      <indexterm>
        <primary>contact lists</primary>
        <secondary>sending
message to</secondary>
      </indexterm>
      <indexterm>
        <primary>messages</primary>
        <secondary>selecting contacts to send to</secondary>
      </indexterm>
      <para>To send a message
to a contact or contact list, select the contact or contact list in a contacts
folder, then choose <menuchoice><guimenu>Actions</guimenu><guimenuitem>Send Message to Contact</guimenuitem></menuchoice>. A <guilabel>Compose a
message</guilabel> window opens, with the contacts in the appropriate field.
Compose the message, then send the message.</para>
    </sect2>
  </sect1>
</chapter>
