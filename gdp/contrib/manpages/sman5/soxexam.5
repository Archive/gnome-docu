<!DOCTYPE REFENTRY PUBLIC "-//Sun Microsystems//DTD DocBook V3.0-Based SolBook Subset V2.0//EN" [
<!--ArborText, Inc., 1988-1999, v.4002-->
<!ENTITY cmd "soxexam">
<!ENTITY % commonents SYSTEM "smancommon.ent">
%commonents;
<!ENTITY % booktitles SYSTEM "booktitles.ent">
%booktitles;
<!ENTITY suncopy "Copyright (c) 2003, Sun Microsystems, Inc. All Rights Reserved.">
]>
<?Pub UDT _bookmark _target>
<?Pub Inc>
<refentry id="soxexam-5">
<!-- %Z%%M% %I% %E% SMI; -->
<refmeta><refentrytitle>soxexam.5</refentrytitle><manvolnum>5</manvolnum>
<refmiscinfo class="date">26 May 2003</refmiscinfo>
<refmiscinfo class="sectdesc">&man5;</refmiscinfo>
<refmiscinfo class="software">&release;</refmiscinfo>
<refmiscinfo class="arch">generic</refmiscinfo>
<refmiscinfo class="copyright">&suncopy;</refmiscinfo>
</refmeta>
<indexterm><primary>soxexam</primary></indexterm><indexterm><primary>sox examples
</primary></indexterm>
<refnamediv id="soxexam-5-name"><refname>soxexam</refname><refpurpose>sox
examples</refpurpose></refnamediv>
<refsect1 id="soxexam-5-desc"><title>&desc-tt;</title>
<para>This reference page provides examples of how to use the <command>sox
</command> command.</para>
</refsect1>
<refsect1 id="soxexam-5-exde"><title>&exde-tt;</title>
<refsect2>
<title>Conversions</title>
<para>The <command>sox</command> command converts an input sound file to a
new file format that uses a similar data type and sample rate. For example,
the following command converts a mono 8000Hz u-law <literal>.au</literal>
file to an 8000Hz u-law <literal>.wav</literal> file:</para>
<screen>example% <userinput>sox monkey.au monkey.wav</userinput></screen>
<para>If an output format does not support the same data type as the input
file, <command>sox</command> selects a default data type for the saved file.
You can override the default data type selection by using command-line options.
This is also useful for producing an output file with higher or lower precision
data or sample rate.</para>
<para>Most file formats that contain headers can automatically be read by
the <command>sox</command> command. When working with headerless files (raw
files), you must use <command>sox</command> command-line options to specify
the data type and sample rate. Alternatively,  you can use the <literal>.ub
</literal>, <literal>.uw</literal>, <literal>.sb</literal>, <literal>.sw</literal>, <literal>
.ul</literal>, and <literal>.sl</literal> pseudo file types. If you use these
filename extensions, you do not have to specify the corresponding options
on the command line.</para>
<para>See the Examples section for more information.</para>
<refsect3>
<title>Precision</title>
<para>The following data types and formats can be represented by their total
uncompressed bit precision. When you convert from one data type to another,
ensure that the data type has an equal or greater precision. Otherwise, the
audio quality is degraded. This may be acceptable if you are working with
topics such as voice audio and you are concerned about disk space or the bandwidth
of the audio data.</para>
<informaltable frame="none">
<tgroup cols="2" colsep="0" rowsep="0"><colspec colname="COLSPEC0" colwidth="50*">
<colspec colname="COLSPEC1" colwidth="50*">
<thead>
<row><entry><para>Data Format</para></entry><entry><para>Precision</para></entry>
</row>
</thead>
<tbody>
<row><entry><para>unsigned byte</para></entry><entry><para>8&ndash;bit</para></entry>
</row>
<row><entry><para>signed byte</para></entry><entry><para>8&ndash;bit</para></entry>
</row>
<row><entry colname="COLSPEC0"><para>A-law</para></entry><entry colname="COLSPEC1"><para>
13&ndash;bit</para></entry></row>
<row><entry><para>u-law</para></entry><entry><para>14&ndash;bit</para></entry>
</row>
<row><entry><para>unsigned word</para></entry><entry><para>16&ndash;bit</para></entry>
</row>
<row><entry><para>signed word</para></entry><entry><para>16&ndash;bit</para></entry>
</row>
<row><entry><para>ADPCM</para></entry><entry><para>16&ndash;bit</para></entry>
</row>
<row><entry><para>GSM</para></entry><entry><para>16&ndash;bit</para></entry>
</row>
<row><entry><para>unsigned long</para></entry><entry><para>32&ndash;bit</para></entry>
</row>
<row><entry><para>signed long</para></entry><entry><para>32&ndash;bit</para></entry>
</row>
</tbody>
</tgroup>
</informaltable>
</refsect3>
</refsect2>
<refsect2>
<title>Effects</title>
<para>The information in this section describes the sound effects provided
by the <command>sox</command> command. Some parameter settings are specified,
so that you can understand the theory by listening to the sound file with
the added effect. When you first use effects, compose the effects as minimally
as possible. Play the sounds in real-time to learn how to tune the parameter
settings for your sounds.</para>
<para>See the Examples section for more information.</para>
<refsect3>
<title>Chorus</title>
<para>The <literal>chorus</literal> effect transforms a single vocal or instrumental
sound into a chorus.</para>
<para>The <literal>chorus</literal> effect is similar to the <literal>echo
</literal> effect with a short delay, but the delay is not constant. The delay
is varied using a sinusoidal or triangular modulation. The modulation depth
defines the range that the modulated delay is played before or after the delay.
The delayed sound will sound slower or faster than the original sound, as
in a chorus where some vocals are a bit out of tune.</para>
<para>The typical delay is around 40 ms to 60 ms, the speed of the modulation
is best near 0.25 Hz, and the modulation depth around 2 ms.</para>
<para>See the Examples section for more information.</para>
</refsect3>
<refsect3>
<title>Compander</title>
<para>The <literal>compander</literal> effect allows the dynamic range of
a signal to be compressed or expanded. For most situations, the attack time
(response to the music getting louder) should be shorter than the decay time
because our ears are more sensitive to suddenly loud music than to suddenly
soft music.</para>
<para>The <literal>transfer</literal> function ensures that very soft sounds
between -90 and -70 decibels remain unchanged. -90 decibels is the limit of
16-bit encoding. This function prevents the compander from boosting the volume
during silent passages such as between movements.  However, sounds in the
range -60 decibels to 0 decibels (maximum volume) are boosted so that the
60-dB dynamic range of the original music is compressed 3-to-1 into a 20-dB
range, which is wide enough to enjoy the music but narrow enough to minimize
noise. The -5 dB output gain is needed to avoid clipping (this value is inexact,
and was derived by experimentation).  Specifying 0 for the initial volume
is acceptable for a clip that starts with silence, and the delay of 0.2 causes
the compander to react more quickly to sudden volume changes.</para>
<para>See the Examples section for more information.</para>
</refsect3>
<refsect3>
<title>Echo</title>
<para>The <literal>echo</literal> effect results in one or more repetitions
of the original sound. The delay is the time difference between the initial
sound and the repeated sound, and the decay is the loudness of the sound.
Multiple echos can have different delays and decays. </para>
<para>See the Examples section for more information.</para>
</refsect3>
<refsect3>
<title>Echos</title>
<para>The <literal>echos</literal> effect creates an "echo in Sequence". That
is, the input for the first echo is the initial sound, the input for the second
echo is the initial sound and the first echo, the input for the third echo
is the initial sound and the first echo and the second echo, and so on. Take
care when using many echoes. A single <literal>echos</literal> has the same
effect as a single <literal>echo</literal>.</para>
<para>See the Examples section for more information.</para>
</refsect3>
<refsect3>
<title>Flanger</title>
<para>The <literal>flanger</literal> effect is similar to the <literal>chorus
</literal> effect, but the delay varies between 0 ms and a maximum of 5 ms.
This effect sounds like the blowing of the wind, sometimes faster and sometimes
slower. The <literal>flanger</literal> effect is widely used in funk and soul
music, where the guitar sound varies and is frequently slow or a bit faster.
</para>
<para>The typical delay is around 3 ms to 5 ms, the speed of the modulation
is best near 0.5 Hz.</para>
<para>See the Examples section for more information.</para>
</refsect3>
<refsect3>
<title>Phaser</title>
<para>The <literal>phaser</literal> effect is similar to the <literal>flanger
</literal> effect, but uses a reverb instead of an echo and does phase shifting.
The delay modulation can be <literal>sinusoidal</literal> or <literal>triangular
</literal>. The <literal>triangular</literal> phaser effect is preferable
for multiple instrument sounds. For single instrument sounds, the <literal>
sinusoidal</literal> phaser effect gives a sharper phasing effect. The decay
should not be too close to 1.0, which will cause dramatic feedback. A good
range for the decay is about 0.5 to 0.1.</para>
<para>See the Examples section for more information.</para>
</refsect3>
<refsect3>
<title>Reverb</title>
<para>The <literal>reverb</literal> effect is often used in small audience
halls or in overcrowded halls, in which the reflection of sound off the walls
is disturbed or dampened. The <literal>reverb</literal> effect provides the
acoustics of a large hall.</para>
<para>When using the <literal>reverb</literal> effect, you must set the walls
or delays such that the sound is realistic. Otherwise, the music may sound
as if it is playing in a tin can or has overloaded feedback which destroys
any illusion of playing in a big hall. To obtain realistic <literal>reverb
</literal> effects, first decide how long the reverb can take place and still
not be loud enough to be registered by your ears. You can do this by varying
the reverb time <literal>t</literal>. To simulate small halls, use 200 ms.
To simulate large halls, use 1000 ms. </para>
<para>Give every wall a delay time. If a wall is too far away for the reverb
time, you will not hear the reverb. The nearest wall is best with a delay
of <literal>t</literal>/4, the farthest wall is best with a delay of <literal>
t</literal>/2. The walls should not stand too close to each other, and should
not be in a multiple-integer distance from each other. Therefore, avoid values
such as 200.0 and 202.0, or 100.0 and 200.0.</para>
<para>See the Examples section for more information.</para>
</refsect3>
<refsect3>
<title>Other Effects</title>
<para>Other effects include <literal>avg</literal>, <literal>band</literal>, <literal>
copy</literal>, <literal>highp</literal>, <literal>lowp</literal>, <literal>
rate</literal>, <literal>stat</literal>, and <literal>vibro</literal>. Experiment
with these effects.</para>
</refsect3>
</refsect2>
</refsect1>
<refsect1 id="soxexam-5-exam"><title>&exam-tt;</title>
<refsect2>
<title>Conversions</title>
<para>Specify the <option>V</option> option each time you run the <command>
sox</command> command, to display the progress of the command. </para>
<example role="example">
<title>Converting From Unsigned Bytes at 8000 Hz to Signed Words at 8000 Hz
</title>
<para><screen>example% <userinput>sox -r 8000 -c 1 filename.ub newfile.sw
</userinput></screen></para>
</example>
<example role="example">
<title>Converting From the Apple AIFF Format to the Microsoft WAV Format</title>
<para><screen>example% <userinput>sox filename.aiff filename.wav</userinput></screen></para>
</example>
<example role="example">
<title>Converting Mono Raw 8000 Hz 8-bit Unsigned PCM Data to a WAV File</title>
<para><screen>example% <userinput>sox -r 8000 -u -b -c 1 filename.raw filename.wav
</userinput></screen></para>
</example>
<example role="example">
<title>Converting Sample Rates</title>
<para>To convert from a sample recorded at 11025 Hz to a u-law file at 8000
Hz sample rate:</para>
<para><screen>example% <userinput>sox infile.wav -t au -r 8000 -U -b -c 1 outputfile.au
</userinput></screen></para>
<para>Downconverting reduces the bandwidth of a sample, but reduces storage
space on your disk. All such conversions are poor and introduce some noise.
You should pass your sample through a lowpass filter prior to downconverting,
to prevent alias signals that sound like additional noise.</para>
</example>
<example role="example">
<title>Adding a Lowpass Filter</title>
<para><screen>example% <userinput>sox infile.wav -t raw -s -w -c 1 - lowpass 3700  |
         sox -t raw -r 11025 -s -w -c 1 - -t au -r 8000 -U -b  -c
     1 ofile.au</userinput></screen></para>
<para>Note the use of <filename>stdout</filename> for output from the first
stage and <filename>stdin</filename> for input to the second stage</para>
</example>
<example role="example">
<title>Reducing the Output Level</title>
<para>If you hear some clicks and pops when converting to u-law or A-law,
reduce the output level slightly. The following example decreases the output
level by 20%:</para>
<para><screen>example% <userinput>sox infile.wav -t au -r 8000 -U -b -c 1 -v .8
     outputfile.au</userinput></screen></para>
</example>
<example role="example">
<title>Using mpg123 to Convert mp3 Files to WAV Files</title>
<para><screen>example% <userinput>mpg123 -b 10000 -s filename.mp3 | sox -t raw -r 44100 -s
     -w -c 2 - filename.wav</userinput></screen></para>
<para>You can use the <command>sox</command> command with other command-line
programs, by using pipes to pass data between the programs.</para>
</example>
<example role="example">
<title>Using the Auto File Format</title>
<para><screen>example% <userinput>sox -V -t auto filename.snd filename.wav
</userinput></screen></para>
<para>The <literal>auto</literal> file format is useful when you are working
with audio data of unknown format. The <literal>auto</literal> file format
guesses the file type. You can then save the audio data in a known audio format.
</para>
<para>It is important to understand how the <command>sox</command> command
works with compressed audio including u-law, A-law, ADPCM, or GSM. The <command>
sox</command> command converts all input data types to uncompressed 32-bit
signed data. The <command>sox</command> command then converts this internal
version into the requested output format. Additional noise may be introduced
by decompressing and then recompressing the data. If applying multiple effects
to audio data, save the intermediate data as PCM data. When applying the final
effect, specify a compressed output format. This approach will keep noise
introduction to a minimum.</para>
</example>
<example role="example">
<title>Applying Various Effects to an 8000 Hz ADPCM Input File</title>
<para><screen>example% <userinput>sox firstfile.wav -r 44100 -s -w secondfile.wav
       sox secondfile.wav thirdfile.wav swap
       sox thirdfile.wav -a -b finalfile.wav mask</userinput></screen></para>
<para>This example results in a 44100 Hz ADPCM output file.</para>
</example>
<example role="example">
<title>Converting Several Audio Files to a New Output Format in a DOS Shell
</title>
<para><screen>example% <userinput>FOR %X IN (*.RAW) DO sox -r 11025 -w -s -t raw $X $X.wav
</userinput></screen></para>
</example>
</refsect2>
<refsect2>
<title>Effects</title>
<para>The examples in this section can be used with any music sample. Use
the <command>play</command> command to listen to sounds instead of looking
at cryptic data in sound files.</para>
<example role="example">
<title>Playing a File in Easy Listening Mode</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> <replaceable>
effect-name</replaceable> <replaceable>effect-parameters</replaceable></userinput></screen></para>
<para><filename>file.<replaceable>xxx</replaceable></filename> indicates that
the file can be of any format.</para>
</example>
<example role="example">
<title>Playing a File in sox-Like Mode for "dsp" Output on a UNIX/Linux Computer
</title>
<para><screen>example% <userinput>sox file.<replaceable>xxx</replaceable> -t ossdsp -w -s /dev/dsp <replaceable>
effect-name</replaceable>
     <replaceable>effect-parameters</replaceable></userinput></screen></para>
</example>
<example role="example">
<title>Playing a File for "au" Output</title>
<para><screen>example% <userinput>sox file.<replaceable>xxx</replaceable> -t sunau  -w  -s  /dev/audio  <replaceable>
effect-name</replaceable>
     <replaceable>effect-parameters</replaceable></userinput></screen></para>
<para>Additional options can be specified. However, in this case, for real-time
playing you will need a very fast machine.</para>
</example>
<example role="example">
<title>Playing a File for Date Freaks</title>
<para><screen>example% <userinput>sox file.<replaceable>xxx</replaceable> file.<replaceable>
yyy</replaceable> <replaceable>effect-name</replaceable> <replaceable>effect-parameters
</replaceable></userinput></screen></para>
</example>
<refsect3>
<title>Echo</title>
<para></para>
<example role="example">
<title>Doubling the Number of Instruments Playing in the Same Sample</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> echo 0.8 0.88 60.0 0.4
</userinput></screen></para>
</example>
<example role="example">
<title>Doubling the Number of Instruments Playing in the Same Sample Using
a Short Delay</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> echo 0.8 0.88 6.0 0.4
</userinput></screen></para>
<para>The output sounds like a metallic robot playing music.</para>
</example>
<example role="example">
<title>Doubling the Number of Instruments Playing in the Same Sample Using
a Longer Delay</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> echo 0.8 0.9 1000.0 0.3
</userinput></screen></para>
<para>The output sounds like an open-air concert in the mountains.</para>
<para>To add one more mountain:</para>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> echo 0.8 0.9 1000.0 0.3 1800.0 0.25
</userinput></screen></para>
</example>
</refsect3>
<refsect3>
<title>Echos</title>
<example role="example">
<title>Bouncing a Sample Twice in Symmetric Echos</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> echos 0.8 0.7 700.0 0.25 700.0 0.3
</userinput></screen></para>
<para></para>
</example>
<example role="example">
<title>Bouncing a Sample Twice in Asymmetric Echos</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> echos 0.8 0.7 700.0 0.25 900.0 0.3
</userinput></screen></para>
<para></para>
</example>
<example role="example">
<title>Playing a Sample as if Played in a Garage</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> echos 0.8 0.7 40.0 0.25 63.0 0.3
</userinput></screen></para>
<para></para>
</example>
</refsect3>
<refsect3>
<title>Chorus</title>
<example role="example">
<title>Using a Single Delay</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> chorus 0.7 0.9 55.0 0.4 0.25 2.0 -t
</userinput></screen></para>
<para>In this example, the sample sounds overloaded.</para>
</example>
<example role="example">
<title>Using Two Delays of the Original Samples</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> chorus 0.6 0.9 50.0 0.4 0.25 2.0 -t 60.0
     0.32 0.4 1.3 -s</userinput></screen></para>
</example>
<example role="example">
<title>Using a Big Chorus of the Sample</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> chorus 0.5 0.9 50.0 0.4 0.25 2.0 -t 60.0
     0.32 0.4 2.3 -t 40.0 0.3 0.3 1.3 -s</userinput></screen></para>
</example>
</refsect3>
<refsect3>
<title>Flanger</title>
<example role="example">
<title>Grooving a Sample</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> flanger 0.6 0.87 3.0 0.9 0.5 -s
</userinput></screen></para>
<para>Listen carefully to hear the difference between sinusoidal and triangular
modulation:</para>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> flanger 0.6 0.87 3.0 0.9 0.5 -t
</userinput></screen></para>
<para>If the decay is a bit lower, the effect sounds more popular:</para>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> flanger 0.8 0.88 3.0 0.4 0.5 -t
</userinput></screen></para>
<para>An unusual loudspeaker system:</para>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> flanger 0.9 0.9 4.0 0.23 1.3 -s
</userinput></screen></para>
</example>
</refsect3>
<refsect3>
<title>Reverb</title>
<example role="example">
<title>Achieving a Reverb Effect with One Wall</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> reverb 1.0 600.0 180.0
</userinput></screen></para>
<para>To add another wall:</para>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> reverb 1.0 600.0 180.0 200.0
</userinput></screen></para>
<para>To add two more walls:</para>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> reverb 1.0 600.0 180.0 200.0 220.0 240.0
</userinput></screen></para>
<para>To achieve the effect of a futuristic hall with six walls:</para>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> reverb 1.0 600.0 180.0 200.0 220.0 240.0
     280.0 300.0</userinput></screen></para>
<para>If you run out of machine power or memory, stop as many applications
as possible. Every interrupt consumes a lot of the CPU time that is absolutely
necessary for bigger halls.</para>
</example>
</refsect3>
<refsect3>
<title>Phaser</title>
<example role="example">
<title>Playing a File Using a Phaser Effect</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> phaser 0.8 0.74 3.0 0.4 0.5 -t
</userinput></screen></para>
<para>This command takes a parameter setting similar to the <literal>flanger
</literal> example, but <replaceable>gain-out</replaceable> is lower because
feedback can raise the output dramatically.</para>
<para>To add an unusual loudspeaker system:</para>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> phaser 0.9 0.85 4.0 0.23 1.3 -s
</userinput></screen></para>
<para>The following example is a popular sample sound:</para>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> phaser 0.89 0.85 1.0 0.24 2.0 -t
</userinput></screen></para>
<para>The following sample sounds as if you have springs in your ears:</para>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> phaser 0.6 0.66 3.0 0.6 2.0 -t
</userinput></screen></para>
</example>
</refsect3>
<refsect3>
<title>Compander</title>
<example role="example">
<title>Playing a File Using a Compander Effect</title>
<para><screen>example% <userinput>play file.<replaceable>xxx</replaceable> compand 0.3,1 -90,-90,-70,-70,-60,-
     20,0,0 -5 0 0.2</userinput></screen></para>
<para>For example, suppose you are listening to music in a noisy environment.
If you turn up the volume enough to hear the soft passages, the loud sections
will be too loud. </para>
</example>
</refsect3>
<refsect3>
<title>Changing the Rate of Playback</title>
<para>You can use <literal>stretch</literal> to change the rate of playback
of an audio sample while preserving the pitch. Other related options are <literal>
speed</literal> to change the speed of play and change the pitch accordingly,
and <literal>pitch</literal> to alter the pitch of a sample.</para>
<example role="example">
<title>Playing the Sample at Half the Speed</title>
<para><screen>example% <userinput>play file.wav stretch 2</userinput></screen></para>
</example>
<example role="example">
<title>Playing the Sample at Twice the Speed</title>
<para><screen>example% <userinput>play file.wav stretch .5</userinput></screen></para>
</example>
<example role="example">
<title>Speeding Up a Sample so that the Sample Plays in Half the Time</title>
<para><screen>example% <userinput>play file.wav speed 2</userinput></screen></para>
</example>
<example role="example">
<title>Raising the Pitch of a Sample 1 <?Pub Caret>While Note (100 cents)
</title>
<para><screen>example% <userinput>play file.wav pitch 100</userinput></screen></para>
</example>
</refsect3>
</refsect2>
</refsect1>
<refsect1 id="soxexam-5-attr"><title>&attr-tt;</title>
<para>See <olink targetdocent="REFMAN5" localinfo="attributes-5"><citerefentry>
<refentrytitle>attributes</refentrytitle><manvolnum>5</manvolnum></citerefentry></olink>
for descriptions of the following attributes:</para>
<informaltable frame="all">
<tgroup cols="2" colsep="1" rowsep="1"><colspec colname="COLSPEC0" colwidth="1*">
<colspec colname="COLSPEC1" colwidth="1*">
<thead>
<row><entry align="center" valign="middle">ATTRIBUTE TYPE</entry><entry align="center"
valign="middle">ATTRIBUTE VALUE</entry></row>
</thead>
<tbody>
<row><entry><para>Availability</para></entry><entry><para>SUNWgnome-sound-recorder
</para></entry></row>
<row><entry colname="COLSPEC0"><para>Interface stability</para></entry><entry
colname="COLSPEC1"><para>External</para></entry></row>
</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="soxexam-5-also"><title>&also-tt;</title>
<!--Reference to another man page-->
<!--Reference to a Help manual-->
<!--Reference to a book.-->
<para><citerefentry><refentrytitle>sox</refentrytitle><manvolnum>1</manvolnum>
</citerefentry>, <citerefentry><refentrytitle>play</refentrytitle><manvolnum>
1</manvolnum></citerefentry>, <citerefentry><refentrytitle>rec</refentrytitle>
<manvolnum>1</manvolnum></citerefentry></para>
</refsect1>
<refsect1 id="soxexam-5-note"><title>&note-tt;</title>
<para>Updated by Ghee Teo, Sun Microsystems  Inc., 2003. Written by Juergen
Mueller (jmueller@uia.ua.ac.be).</para>
</refsect1>
</refentry>
<?Pub *0000025876>
