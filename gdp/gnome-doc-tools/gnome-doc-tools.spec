Summary: Extra tools for GDP members
Name: gnome-doc-tools
Packager: David C Mason
%define version 1.0
%define release 3
Version: %{version}
Release: %{release}
Copyright: 2000 Red Hat, Inc.
Distribution: Unsupported RPM for Gnome Documentors
Prereq: sgml-common
Source: http://people.redhat.com/dcm/gnome-doc-tools-%{version}.tar.gz
Group: Utilities/Text
BuildArch: noarch
BuildRoot: /var/tmp/%{name}-%{version}-%{release}-root
Prefix: /usr

%description
The GNOME Documentation Project has a few tools, scripts and files necessary for creating documentation for GNOME. 

%prep
%setup
%build
./configure --prefix=/usr

%install
make install DESTDIR=$RPM_BUILD_ROOT

%clean 
rm -rf $RPM_BUILD_ROOT

%post
V=%{version}-%{release}
%{prefix}/bin/install-catalog --install png-support --version $V
mv %{prefix}/lib/sgml/stylesheets/cygnus-both.dsl %{prefix}/lib/sgml/stylesheets/cygnus-both.dsl.bak
cp %{prefix}/lib/sgml/stylesheets/gdp-both.dsl %{prefix}/lib/sgml/stylesheets/cygnus-both.dsl

%postun
# since old-postun is run *after* new-post, we must always cycle.
V=%{version}-%{release}
%{prefix}/bin/install-catalog --remove png-support --version $V
mv %{prefix}/lib/sgml/stylesheets/cygnus-both.dsl.bak %{prefix}/lib/sgml/stylesheets/cygnus-both.dsl

%files
/usr/lib/sgml/png-support-3.0.dtd
/usr/lib/sgml/png-support-3.1.dtd
/usr/lib/sgml/png-support.cat
/usr/lib/sgml/stylesheets/gdp-both.dsl
/usr/bin/png2eps
/usr/bin/dbnochunks
/usr/man/man1/png2eps.1 
/usr/man/man1/dbnochunks.1

%changelog 
* Sat Aug 19 2000 Alan Cox
- cleaned up errors when building

* Thu Apr 27 2000 David C Mason 
- updated the gdp-both.dsl
for cleaner printing capabilities. Fixed typo in README

* Thu Apr 20 2000 David C Mason
- initial build
