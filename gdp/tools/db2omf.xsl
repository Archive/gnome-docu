<?xml version='1.0'?>
<!-- vim:set sts=2 shiftwidth=2 syntax=sgml: -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	        xmlns:omf="http://metalab.unc.edu/osrt/omf/"
                version='1.0'>

<xsl:strip-space elements="*"/>

<xsl:output method="xml"
            encoding="ISO-8859-1"
	    indent="yes" />

<xsl:template name="copy">
  <xsl:copy>
    <xsl:for-each select="./@*">
      <xsl:copy/>
    </xsl:for-each>
    <xsl:apply-templates />
  </xsl:copy>
</xsl:template>

<xsl:template match="copyright">
  <xsl:variable name="fdl" select='/article/appendix[@id="gfdl"]' />
  <xsl:element name="rights">
    <xsl:element name="holder">
      <xsl:attribute name="name">
	<xsl:value-of select="./holder" />
      </xsl:attribute>
    </xsl:element>
    <xsl:if test="$fdl">
      <xsl:element name="licence">
	<xsl:attribute name="name">
	  <xsl:value-of select="$fdl/title" />
	</xsl:attribute>
	<xsl:attribute name="version">
	  <xsl:value-of select="substring ($fdl/para[1], 9, 3)" />
	</xsl:attribute>
	<xsl:attribute name="url">http://www.gnu.org/copyleft/fdl.txt</xsl:attribute>
      </xsl:element>
    </xsl:if>
  </xsl:element>
</xsl:template>

<xsl:template match="author">
  <xsl:element name="person">
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<xsl:template match="firstname">
  <xsl:element name="firstName">
    <xsl:value-of select="."/>
  </xsl:element>
</xsl:template>

<xsl:template match="surname">
  <xsl:element name="lastName">
    <xsl:value-of select="."/>
  </xsl:element>
</xsl:template>


<xsl:template match="affiliation/address">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="affiliation/address/email">
  <xsl:call-template name="copy" />
</xsl:template>

<xsl:template match="abstract/para[1]">
  <xsl:element name="description">
    <xsl:value-of select="." />
  </xsl:element>
</xsl:template>

<xsl:template match="omf:resource//node()">
  <xsl:call-template name="copy"/>
</xsl:template>

<xsl:template match="article">
<omf>
  <resource>
    <title><xsl:value-of select="articleinfo/title"/></title>
    <creator>
      <xsl:apply-templates select="articleinfo/author" />
    </creator>
    <xsl:apply-templates select="articleinfo/copyright" />
    <xsl:apply-templates select="articleinfo/abstract" />
    <xsl:apply-templates select="omf:resource/*" />
    <format dtd="DocBook" mime="text/xml" />
  </resource>
</omf>
</xsl:template>

</xsl:stylesheet>
