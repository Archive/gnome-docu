#!/usr/bin/perl -w

%FILELIST = ('gnome-faq.tar.gz' => 'gnome-faq',
	     'tutorial.tar.gz' => 'tutorials/gnome-app-programming',
	     'programming-guidelines.tar.gz' => 'white-papers/ProgrammingGuidelines',
	     'Components.tar.gz' => 'white-papers/Components',
	     'canvas.tar.gz' => 'white-papers/Canvas',
	     'white-papers.sgml' => 'white-papers',
	     'writing-white-papers.tar.gz' => 'white-papers/tutorial',
	     'libroadmap.tar.gz' => 'white-papers/LibRoadMap',
	     'gtki18n.tar.gz' => 'white-papers/i18n',
	     'DND.tar.gz' => 'white-papers/DND',
	     'esd.tar.gz' => 'white-papers/esd'
	    );

if ($#ARGV == -1) {
  foreach (keys %FILELIST) {
    print $FILELIST{$_}."/".$_."\n";
  }
} else {
  foreach (@ARGV) {
    print $FILELIST{$_}."/".$_."\n";
  }
}
