#!/usr/bin/perl -w

%FILELIST = ('other.tar.gz' => 'white-papers'
	    );

if ($#ARGV == -1) {
  foreach (keys %FILELIST) {
    print $FILELIST{$_}."/".$_."\n";
  }
} else {
  foreach (@ARGV) {
    print $FILELIST{$_}."/".$_."\n";
  }
}
