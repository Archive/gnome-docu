#!/home/martin/PERL/bin/perl -w

use strict;
require 5.0;

$_ = join '', <>;

s,\\&,,g;
s,<,&lt;,g;
s,>,&gt;,g;
#s,@,&at;,g;

s,^.P$,\n<para>,gm;
s,^.H\s+(\d+)\s+\"?(.*?)\"?\.?$,<sect$1>\n<title>$2</title>,gm;
s,^.HU\s+\"?(.*?)\"?\.?$,<sect1>\n<title>$1</title>,gm;
s,^.B\s+([-\w]+)\s*\"\s+-\s+(.*?)\"$,<term>$1</term>\n<listitem>\n<para>$2</para>\n</listitem>,gm;
s,\fB(.*?)\fR,<emphasis>$1</emphasis>,g;
s,^.VERBON\s+(\d+)$,\n<informalexample><%programlisting>,gm;
s,^.VERBOFF$,</%programlisting></informalexample>,gm;

s,^.BL$,\n<variablelist>,gm;
s,^.LI$,<varlistentry>,gm;
s,^.LE$,</variablelist>,gm;

while (m,<varlistentry>(.*?)(?=<varlistentry>|</variablelist>),sg) {
  next if $& =~ /<(term|para|listitem)>/;
  $_ = qq[$`<varlistentry>\n<para>$1$'];
}

while (m,<%programlisting>(.*?)</%programlisting>,sg) {
  my ($pre,$match,$post) = ($`,$1,$'); $match =~ s,&,&amp;,g;
  $_ = qq[$pre<programlisting>$match</programlisting>$post];
}

my ($AUTHOR_FIRST, $AUTHOR_SURNAME, $AUTHOR_EMAIL) =
  /^.AU\s+\"?\s?(\w+)\s+(\w+)\s*&lt;(.*?)&gt;\s*\"?$/sm ? ($1,$2,$3) : ('','','');
my ($ORGANIZATION) = /^.AF\s+\"?\s?(.*?)\s*\"?$/sm ? $1 : '';
my $TITLE = /^.TL(.*?)(?=\n\.)/sm ? $1 : '';
my $ABSTRACT = /^.AS\n(.*?)\n.AE$/sm ? $1 : '';
my $YEAR = 1999;

$TITLE =~ s,^\s*(.*?)\s*$,$1,s;

s,^.nr\s+(.*)\n,,gm;
s,^.COVER\n(.*?)\n.COVEND$,,sm;
s,\\fB(.*?)\\fR,<emphasis>$1</emphasis>,g;
s,^.TC\s+(.*)\n,,gm;

print qq{<!doctype book PUBLIC "-//Davenport//DTD DocBook V3.0//EN">
<book>
<bookinfo>
<title>$TITLE</title>
<authorgroup>
  <author>
    <firstname>$AUTHOR_FIRST</firstname>
    <surname>$AUTHOR_SURNAME</surname>
    <affiliation>
      <orgname>$ORGANIZATION</orgname>
      <address>
        <email>$AUTHOR_EMAIL</email>
      </address>
    </affiliation>
  </author>
</authorgroup>
<copyright>
  <year>$YEAR</year>
  <holder>$AUTHOR_FIRST $AUTHOR_SURNAME</holder>
</copyright>

<abstract>
<para>
$ABSTRACT
</abstract>
</bookinfo>

<chapter>
<title>$TITLE</title>
};

print $_;

print qq{</book>\n};
