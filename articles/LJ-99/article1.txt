\documentstyle[12pt]{article}
\begin{document}

\author{
	George Lebl (\tt jirka@5z.com) \\ 
	Elliot Lee ({\tt sopwith@redhat.com}) \\ 
	Miguel de Icaza ({\tt miguel@gnu.org})
}
\title{GNOME, its state and the future of it.}
\maketitle

\section {Computing Made Easy}

The GNOME Project is aimed at making Unix attractive and easy to use.
To help achieve the goals of the GNU project, we want to make sure
that users are presented with a full suite of applications, as well as
a desktop that enable users to manage their computers effectively.
The GNOME team has been focusing on creating a reusable infrastructure
of development libraries and tools, as well as productivity
applications based on this infrastructure.

\section{GNOME History}

The existing graphical user interfaces (GUIs) were lagging behind in
usability on the desktop.  They were also mostly proprietary and thus
unusable on a GNU system.  Because of this, the free systems suffered
the most by this lack of a simple GUI.  There was a general perception
of a Unix and GNU/Linux system as being a experts-only system which
was hard to use.  

Then came GIMP, a full featured application that would one expect on a
desktop system.  In the summer of 1997 the GNOME project was started
to create a free desktop for the GNU project that would enable free
systems to reach desktop users to address the desktop functionality
gap in GNU systems.  

Two years later, in March of 1999, GNOME has achieved it's primary
goal of making basic tasks easy for the desktop user with the release
of 1.0.  

After this point, we released updated packages pretty often, following
the Linux-kernel model: as soon as a batch of bugs were fixed, as soon
as some major improvements were done, we released the individual
packages to the public. 

Seven months later, in October 1999, a coordinated released was done:
it was an enhanced, improved code-wise and documentation-wise, many
bugs that had been reported were fixed and the release process was
managed by Elliot Lee.

This release was called "October GNOME".  In the process of creating a
desktop environment for end users, GNOME also created a powerful
development environment which makes it easy to create consistent,
well-integrated applications that are easy to use.

There are still many gaps left in the desktop arena, and in the
productivity applications area, and the free software community is
working hard to change this. 

\section{GNOME Overview}

The goals of the GNOME Project can be divided into three areas:

	First, a full-featured desktop environment.
	
	Second, a set of inter-operable applications with a
	consistent, easy to use interface.
	
	Third, a powerful application development framework.
	
\subsection{The Desktop}

The desktop environment is not the set of applications, such as a
web browser or a spreadsheet, with which a user interacts with the 
system to do useful tasks, but the utilities which provide the 
user with the choice of control over the working environment. 
As the most immediately apparent part of GNOME, the desktop
environment includes the file manager, the panel, and the help browser,
as well as other utilities necessary for the day-to-day maintenance
of one's computing environment.

The GNOME "session" begins with the GNOME Display Manager: this one
grants you access to the system.  The beauty of this process is that
Martin (the author of Gdm) re-did the whole login sequence to be secure
and extensible.  The gdm code base is designed to be robust and
secure.  I know this is of not much interest from the desktop point of
view, but it is a niece piece of work, and many times ignored. 

From there, the GNOME panel, and the GNOME file manager provide the
desktop functionality to let users launch applications and manage
their information.

The GNOME desktop was the first desktop to include application themes.
Application themes are a way to make the applications to look
different.  People can choose to make their desktop look like other
popular systems, or tune it to suit their needs and personal interface
desire.  The next major release of GNOME will include a new and
updated theme mechanism that will be better integrated and will
include theme packages that will affect the entire desktop, not only
the applications.

We also integrate the GNOME personal information management system
(calendar, address book, task list) with the Palm Pilot and more
systems can be plugged into the system.  To learn more about this
feature, check {\tt http://www.gnome.org/gnome-pilot}. 

\subsection{The GNOME Workshop}

Just being able to choose a screen-saver, organize icons, browse
application menus, and move files doesn't mean you are a productive
member of society.  What users want is a set of applications helping
them accomplish actual work.  This is where the GNOME Workshop project 
comes in.  It is recognized that there are many applications out there
which were not done by the core GNOME team, but which would be much more
useful if they were all integrated with each other and with the
desktop.  The GNOME Workshop project wants to make a set of highly
integrated applications that do what you need, whether it is managing
finances, writing letters, or editing a picture.  Components of GNOME
Workshop that already have reached a functional state include a
highly-capable spreadsheet (Gnumeric), a word processing application
(AbiWord), and an image editing application (the famous GIMP).  Other
component applications are coming along quickly, and news of their
releases will be listed on the GNOME Workshop home page.

\subsection{ Developer Goodies}

Another important part of GNOME is the development environment.  UNIX
has not had a history of applications with a consistent and powerful
graphical interface.  The few graphical applications that existed all
behaved a little differently, looked a little differently, did not
usually have a powerful interface, and were not easy to write for their
developers.  GNOME addresses this need by simplifying the
development of applications, allowing the creation of easy-to-use and
powerful graphical interfaces.

GNOME provides a high-level application framework which frees the
programmer from having to worry about the low-level details of
graphical application interfaces, allowing concentration on the actual
application.  Glade, a tool for user interface design used by many GNOME
applications, takes this concept a step further by allowing graphical
creation of a program's user interface.  The Libglade library
allows user interfaces to be created at runtime from the XML interface
description files saved by Glade.

GNOME goes far beyond being a simple GUI developers tool, it simplifies
application tasks, so that your applications are easier to write,
maintain and integrate well with others.  GNOME also recognizes that
not every programming language is useful for every kind of job.  

We paid special attention to making the GNOME APIs easy to wrap
and export to other programming languages, to let people develop
their GNOME-based applications in their language of their choice. 

So in addition to C, in which the core GNOME libraries are written,
there are bindings for many languages, including C++, Objective C,
Guile, Python, Perl, Ada95, Tom, Pascal, Haskell and others.  Java
bindings are in development, so coupled with gcc's ability to compile
Java code, Java may become a very viable alternative for GNOME
programming.

\section{The GNOME Workshop components}

\subsection {The Gnumeric spreadsheet}

The objectives of the Gnumeric spreadsheet are to include all the
features you can find on proprietary spreadsheets.  Gnumeric has
implemented pretty much all of the built-in functions available in
Excel 2000 and by the time you read this, it should be complete.

Users can write their own functions in Python, Perl or Scheme and
access them trough the Gnumeric engine.  This enables people to adapt
Gnumeric to their needs in little time.  Of course, if you like to use
C, C++ or assembly, you can also hook up your functions to the
spreadsheet. 

We are copying as many good ideas and as many usability features from
popular spreadsheets and including them into Gnumeric.

Gnumeric also serves as a test-bed for the various GNOME
technologies: CORBA, the Bonobo component model and document model and
the GNOME printing architecture.  Gnumeric was also one of the first
applications that used libxml (and indeed, the Gnumeric native file
format is XML-based). 

Gnumeric's rich set of import and export filters make it a quite
valuable addition to your tool-box, and if you are looking for an
application to load your Excel spreadsheets, look no further: Gnumeric
has the best Excel importing functionality available for GNU/Linux as
tested by LinuxWorld.

Not only you can define your own functions for spreadsheet in the
languages mentioned above, but you can actually script the entire
application from any language that supports CORBA and control the
spreadsheet remotely.  Anything that the user can do by using the
spreadsheet can be done trough a CORBA communications channel,
enabling you to use Gnumeric as a reusable engine that your
applications can use. 


\subsection{The AbiWord word processor}

AbiWord is a project lead by Source-Gear.  AbiWord is a cross platform 
word processor that can import your Word files.  The word processor
has gained a lot of attention from many communities as they can run
the same word processor across Unix, Win32, BeOS and MacOS.

Their web site is at {\tt http://www.abisource.com}, you can download
it from there. 

Abi contains a number of interesting features found in the proprietary
word processing equivalents.

\subsection{The GIMP}

The GIMP is the de-facto standard for image editing, photo retouching,
image authoring and composition.  It is hard to list all the features
of the GIMP, as it is one of the most successful free software
projects.

It is an extremely capable piece of software with many
capabilities. It can be used as a simple paint program, a expert
quality photo retouching program, an online batch processing system, a
mass production image renderer and more.

GIMP is extremely expandable and extensible. It is designed to be
augmented with plug-ins and extensions to do just about anything. The
advanced scripting interface allows everything from the simplest task
to the most complex image manipulation procedures to be easily
scripted.

GIMP is one of the most actively developed applications and Yosh
Manish acts as its maintainer and coordinator.

\subsection{Dia}

Dia is a diagram application that can be used as a replacement of the
popular proprietary application "Vision".   Dia supports various kinds
of high-level objects that you can use to draw with: UML, Network,
databases, circuit objects, flowcharts and more.

Dia is easy to extend with new object collections as the various
objects are defined using an XML-based file format.  

It has quickly become the tool of choice for GNOME developer to do
diagrams with and communicating graphical information with other
developers.  The Dia team consists of seven programmers and it is
commanded by Alexander Larsson.  

The Dia community is very active.

\subsection{GNOME DB}

GNOME DB is a framework for creating database applications.  It
provides a common API with pluggable back ends to various database
sources as well as various specialized widgets for handling various
database tasks.  The backends in the typical GNOME tradition are based
on CORBA.

Michael Lausch and Rodrigo Moya are the main developers of this
project. 

GNOME-DB is composed of three separated, independent layers. The
lower-level one are the database servers, which are CORBA servers that
map the database-specific APIs to the GDA model. The middle-layer is
the GDA client library, which sits on top of the CORBA interface to
allow an easier access to client applications, along with the GDA UI
library, which provides a way of easily accessing the client library
from GNOME applications. The upper-level layer is composed by the
applications making use of the middle-level layer, such as, for
example, the rolodex application and the SQL Front End.

\subsection{Gill and Eye of Gnome}

Gill (the GNOME Illustrations program) and Eye of GNOME (the GNOME
image viewer and organizer) are the most recent additions to the suite
of productivity applications for GNOME.

Gill is particularly interesting as its native file format is the Web
Consortium's Scalable Vector Graphics (SVG).  And internally the
applications has been since day one organized around a DOM (Also a W3C
standard).  Gill is still in its infancy, but given that it is based
on a strong foundation, it is already pretty powerful display-wise.

Eye of Gnome is also in its infancy, but it has served as a test-bed
for various new GNOME technologies (our new imaging system, various
new GNOME Canvas improvements, and the new model-view-controller based
widgets). 

It is our image viewer of choice right now, and it is amazingly fast
too. 

\section{Development tools}

\subsection{CORBA and Bonobo.}

The lack of a standard, and freely available setup for component
programming is a problem present in the UNIX world nowadays.  One of
the issues being addressed by the GNOME project is providing such a
framework.  The GNOME framework is based on the CORBA[omg] object
model.  During the design of the GNOME component interfaces, we tried
to address a range of needs:

	\item {Automation:} Allow applications to be remotely
	controlled.  People should be able to launch and control GNOME
	applications remotely.  This is achieved by the GNOME Object
	Activation Directory (GOAD), the supporting libraries (the
	GNORBA libraries) and making use of the CORBA facilities for
	binding CORBA to scripting languages.

	\item {Compound document creation:} It is important to design
	and implement the GNOME applications in such a way that they
	will let the user create compound documents:  Documents whose
	contents might have been produced by different tools. 

	\item {In-place document editing:} The next step to compound
	document creation is providing ways to edit embedded documents
	inside a container application.  This means that it should be
	possible to make changes to an embedded spreadsheet document
	inside a word processing document in a seamless fashion:  it
	is important to make this integration simple and easy to use. 

	\item {Component reuse:} Filters and pipes proved to be 
	important building blocks in Unix but they are very limited:
	The flow of control usually goes in a single direction and the
	protocol used to exchange information is very simple for
	today's needs.  

	\item {Desktop integration:} The GNOME desktop deals with
	CORBA interfaces to services:  This means that as far as an
	application is concerned, they only care about the published
	interface that a program provides.

	In GNOME, interfaces for specific tasks play an important
	role: it is up to the user to choose which implementation of
	those interfaces he uses (for example, the Mailer interface is
	implemented by the GNOME Balsa mail reader, but it can be
	implemented by Emacs RMAIL, Emacs GNUS or the Mozilla Mail
	Reader:  if any of those provide the CORBA Mailer interface
	they will fit and integrate with the GNOME desktop). 

All of these problems can be fixed by the use of CORBA, an OMG
standard ({\tt http://www.omg.org}).

We are using ORBit as our CORBA implementation with very good results.
ORBit was designed to be small, fast, robust, reliable and efficient.

Many times people hear the word "CORBA" and they immediately think
"bloat".

This is not the case with ORBit.  ORBit is thin, for most applications
the working set of ORBit's ORB is around 30k, which makes it suitable
for embedding in pretty much every application.  And this is pretty
exciting, because applications (As Gnumeric and a few others) export
their internals to the desktop as highly specialized reusable
components (think "Unix filters on steroids" here).  

Also, CORBA has proved to be very useful, as we can use it to run
regression tests on our applications from a Perl script (using Owen
Taylor's wonderful CORBA-Perl ({\tt
http://people.redhat.com/otaylor/corba/orbit.html}).  

This just happens to be one of our favorite CORBA bindings for a
scripting language, but you can get CORBA bindings for pretty much
every language.

By using CORBA as our foundation, we ensure interoperability with
existing systems, and anyone that supports CORBA can talk to our
applications.  

CORBA in GNOME plays the same role that COM plays in the world of
Microsoft Windows.

\subsection{Component programming in GNOME:}

This section has been taken from the Bonobo documentation:

	Modern applications are increasingly functional and more
	powerful.  As these applications grow in terms of
	functionality and features, the internal complexity of
	these applications increases as well.
	
	As applications become larger, the task of maintaining the
	code becomes harder. Re-targeting the internals of
	applications takes more programmer time, changing the code
	becomes more difficult, and maintaining compatibility takes a
	larger amount of time from the core developers to push an
	application in a new direction.

	Adding complexity to a software project is extremely
	simple, and once the projects goes down that road, the
	barrier for free software programmers to contribute to a
	given project is increased.  Once the complexity barrier
	has been raised, the time a developer needs to get
	acquainted with the code and feel comfortable with the
	over-all architecture to start contributing is raised
	forever.

	Given the nature of the GNOME system, being a system
	that is extremely large and consists of multiple applications,
	it is very important for us to keep the code as simple as
	possible.  A simple, clean and correct design takes
	precedence over other constraints.

	Software created by the free software community has been
	historically done by hobbyists on their spare time working
	on the things they are interested in.  Unlike other
	software projects, the contributions of individuals to a
	project can disappear and stop contributing at any point,
	without further notice.

	The Bonobo component system has been designed to solve
	these problems and the problems derived from the facts
	outlined above.

	Component software helps reducing the complexity of
	applications by reducing the amount of information a
	programmer needs to know about the system.  This is
	achieved by making each software component implement a
	very well-defined interface (note: these interfaces are
	CORBA interfaces specified in the CORBA Interface
	Definition Language).
	
	Component software also enabled programmers to build
	larger, more complex applications by gluing different
	components together into bigger applications.  For
	example, you can use a Gnumeric spreadsheet component for
	entering tabular data in your application.  You can use an
	HTML display component to embed HTML text into a region
	in your application; You can embed a plotting component
	to add graphic capabilities to your application; You can
	use an structure vector graphics (SVG) component to
	display, edit and manipulate SVG files into your
	application.

	Bonobo is the GNOME foundation for writing and
	implementing reusable software Components.  Components are
	pieces of software that provide a well-defined interface
	and are designed to be used in cooperation with other
	components.  In the Bonobo universe, CORBA is used as the
	communication layer that binds components together.

	The magic behind component programming is in the
	"interfaces" that component export to the world.  Each one
	of these interfaces is a "socket" into which other
	components and applications can plug into.  These
	interfaces are defined in terms of CORBA interfaces.

	One added benefit of basing the component system on top of
	CORBA is that programs written in any language can use
	components written in any other language.  The only
	requirement is that the components agree on the CORBA
	interfaces used.  In the case of the Bonobo architecture,
	we have provided a number of interfaces that define the
	interactions required for this to work.  By having
	components implement the Bonobo interfaces, they will work
	along any other Bonobo-based components.

\subsection{What does Bonobo provide for an application developer?}

Bonobo in short provides the following features to an application
developer:

	\item An infrastructure for creating reusable components
	(either as full blown processes, shared libraries or remote
	processes).

	\item An infrastructure for reusing existing components (you
	can reuse various parts of the the Gnumeric spreadsheet in
	your application.  For example, you can use Gnumeric to
	provide data-entry facilities in your own application, or use
	it as a computation engine).

	\item An infrastructure for creating persistent controls.

	\item An infrastructure for creating compound documents:  Not
	only reusing existing applications is interesting in a
	vertical-application, but also the possibility of creating
	single documents that are composed of various parts:
	spreadsheets, equations, graphics and so on.


\subsection{Image handling}

GNOME 1.0 utilized the Imlib software library for image loading and
manipulation. While Imlib fits the needs of some applications, its
design does not mesh well with the typical GNOME use case.  As a
replacement, libart, an RGBA image manipulation library, and GdkRGB,
an API to allow high-performance display of RGB images, are integrated
together in gdk-pixbuf, an image loading library that solves the
problems of Imlib and adds features such as anti-aliasing and
high-quality rendering.

\subsection{Canvas and Libart}

GNOME 1.0 included libart, and the anti aliased canvas, but it was not
used very much and the anti-aliased canvas was marked as unstable.  In
the future GNOME will use libart and the anti-aliased canvas much
more.  The new GNOME panel and GNOME pixmap widget already use libart and
gdk-pixbuf to provide anti-aliased icons.

\subsection{Glade and Libglade}

As mentioned earlier, Glade is a GUI designer which is now being used
in much new GNOME development.  Normally Glade will generate source
code to build the interface that you create.  However, the really
revolutionary and useful way to use Glade is to use it in combination
with Libglade.  

Libglade is a library that will load a saved Glade project and build
the interface for you, on the fly.  This means that you could
conceivably have different interface files for different languages,
application modes or "themes".  It also gives the user the ultimate
power of customization, since the interface of the application can be
modified without any programming skill.  

Various GNOME applications are now designed with Glade, and this
reduces the development time a lot.  It is easy to prototype, easy to
customize and easy to extend.  The joy has arrived.  

\subsection{Printing}

Unix printing is both a blessing and a curse, and while it may be
flexible to use, it has always been hard to set up, and output quality
was often low on non-PostScript printers.  In addition, most
applications didn't even include printing support, because there was
no convenient, unified API for printing on Unix. GNOME Print is a
library which allows developers to easily add printing capabilities to
their applications, and the users to quickly and easily access all the
output parameters through a consistent graphical interface.

The GNOME Print imaging model is based on Postscript, with two
extensions: anti-aliasing and transparency.   Please note that I said
"An imaging model based on Postscript", not "PostScript".  This means
that you have the same, well, imaging model, but the way you print is
by using an API exported to your favorite language.

Currently GNOME Print includes a Postscript driver, an on-screen driver
(for doing previews), meta-file drivers (for storing printed
information, transferring it, render it on a scaled context) and a
generic RGB driver (on which we will build the per-printer real
printer drivers).

As you could expect, we do reuse our technologies: the rasterization
engine used in the canvas, is the same rasterization engine used for
the on-screen preview and for rasterizing the output for the native
printer drivers.

If you are interested in working on the native drivers, we will most
definetly welcome your help.  

\section{Work in progress}

The GNOME Project is not resting on its laurels.  While the 1.0
release series addresses the basic need for a Unix GUI, there are
portions of the existing implementation that need refinement, as well
as additional features that are becoming essential on a modern
desktop.

At the same time, the GNOME 1.0 API will still be supported through
compatibility libraries, so that the migration of existing source code
to the new libraries will be painless. This will allow developers to
focus on the development of applications instead of having to worry
about following GNOME API changes.

\subsection{The new File Manager}

The file manager is being redesigned completely. The new version will
feature a asynchronous, network transparent virtual file-system which
is usable from all applications independent of the file manager.  This
will make it easy to have applications that are network aware, and it 
will make network administration a snap.  This step forward from
traditional virtual file-system libraries, which are not asynchronous,
allows better responsiveness from graphical applications.

\subsection{Panel Improvements}

In addition to the file manager, another highly visible part of the desktop
is the panel.  It is a place to put icons to launch applications, access
application menus, manage open windows and run small utility applications
to give status of the machine, play CD's or display time.  We recognize
that people are not the same, and different people like to work differently.
That is why the panel is highly customizable, and this customization has been
greatly extended in the new version.  The panel supports many new different
modes of operation.  One of these is that you can select a size for your
panels depending on your tastes and screen size.  There are also new modes
to place panels anywhere along an edge, and even anywhere on screen.  In
addition to that all icons on the panel are now anti aliased, and external
applets have the ability to also use anti aliasing for their display.  There
are lot of other smaller additions to make the panel more configurable,
easier to use and to make your GNOME experience more pleasurable.

\subsection{Configuration Data Storage}

Another noticeable improvement coming to GNOME is GConf, a new
configuration API and backend. This will add the features not provided
by the simplistic configuration API in GNOME 1.0.  It will make it
easy to plug in different backends for the actual storage, so that
you can change how and where the data is actually stored without
touching the applications themselves.

\subsection{Object Activation Framework}

Since many GNOME applications utilize CORBA, a framework for locating
and activating these applications is necessary. OAF, the Object
Activation Framework, provides a simple method for finding and running
the CORBA objects available on a computer system. Distributed
operation is supported, allowing activation of objects on a network of
computers being used in a GNOME desktop. The flexibility of OAF
enables it to be used outside of GNOME programs, allowing non-GNOME
CORBA applications to be utilized alongside GNOME applications.

\subsection{Pango: Reaching out to the world}

Supporting the various human languages is a very complex task, not
because of the difficulty, but because of the wide range of
communication systems humans have came up with in the last few
thousand years.  

Our goals in the GNOME project is empowering users and giving them a
chance to run free software, we have to make sure that everyone on
this planet can use our tools with their language, and that our
applications can be used by all people.

Supporting all these human languages has many facets: first of all
applications should be Unicode aware, and we need to make sure the
applications can display text in the various languages as well as
accept input from those languages correctly.

A simplistic approach to this would not be really usable.  If we want
our productivity applications to use this infrastructure, we also need
to make sure that Pango provides all the features that high-end
applications might require.  This includes typographical features as
well as correct hyphenation features. 

The Pango project assembles all of these efforts: creating a Unicode
handling infrastructure, creating the graphical components required to
edit, display characters and glyphs from various languages as well as
providing correct hyphenation and good support for typographical
features like ligatures. 

\subsection{Gtk+}

Gtk+ is the toolkit used by GNOME and the various GNOME applications.
The Gtk+ team lead by Owen Taylor and Tim Janik is making steady
progress towards the Gtk+ 1.4 release.

The major highlights of Gtk+ 1.4 include: flicker-free drawing,
better internationalization support (trough Pango), and it will
integrate the BeOS and Win32 ports.

Applications written against the Gtk+ 1.4 and GNOME APIs, will be
portable to Windows and BeOS (keep in mind that GNOME/Gtk+
applications talk to a windowing system layer called Gdk, which is
independent of X11;  This is why it is simple to port them to other
architectures). 

\section{User interface}

The programmers that brought you GNOME, are programmers, and most of
the time they are not graphic designers, and did not have all the
experience required to make the best user interface possible.

It is hard to write good user interfaces, and we are trying to address
this.  The GNOME User Interface team is responsible for redesigning
the look and feel of various GNOME applications by studying the
current application failures and the good ideas from other
applications and ideas from other systems.


\section{The Importance of Free Software}

GNOME is part of the GNU project, and it is free software (some people
also call this open source software), created for the people by the
people.  We want to make software that grants users various freedoms.

It is not software owned by some large faceless company.  You,
therefore, are the person best qualified to contribute to its
improvement.  Although programming help is extremely welcome, you
don't need to be able to program to help.

Documentation, translations, web site maintenance, packaging, and
graphic design are just a few of the many areas that people are
already making contributions in. If you dislike an aspect of GNOME and
want to see it improved, or if you want to see a totally new feature
added, the way to make this change happen is to start
contributing. 

You might think that any contribution you could make would be
unimportant, but if many people made small contributions, the result
would be a large increase in the progress being made.  Your efforts
for GNOME will continue to make the power of Unix easily accessible to
average users.

\section{Further reading}

If you want to learn more about the technologies available for
developers, you can check our developer web site that contains
articles, white papers, online documentation for the various GNOME
APIs as well as tutorials at {\tt http://developer.gnome.org}

If you want to stay up to date on GNOME-related events, you might want
to subscribe to the gnome-announce-list@gnome.org mailing list, or you
can check our news web site: {\tt http://news.gnome.org/gnome-news}

The GNOME project uses Bonsai and LXR which let you browse the code by
revision numbers, check the changes, see the people involved and cross
reference the entire source base.  To use Bonsai or LXR on the GNOME
code, go to our CVS access web page: {\tt http://cvs.gnome.org}

\section {Getting GNOME}

GNOME is currently distributed by various Linux distributions: Red Hat
Linux and its derivatives, Turbo Linux, Mandrake Linux, and SuSE
Linux.  It should appear soon in Caldera Linux and hopefully others
will follow soon.

You can get precompiled versions of GNOME for various operating
systems from: {\tt http://www.gnome.org/start}, or you can download
the source code directly from: 
{\tt ftp://ftp.gnome.org/pub/GNOME/sources}

People interested in getting the latest development version of GNOME
can use our network of anonymous CVS servers.  For detailed
instructions, see {\tt http://developer.gnome.org/tools/cvs.html}

\section{Helping in GNOME}

There are many ways to help the GNOME project, and this is not limited
only to seasoned programmers.  Everyone can be part of the effort to
make free software accessible to everyone:

     \item {\bf Users:} Stay up-to-date with the latest releases of
	   the GNOME libraries by subscribing to GNOME Mailing Lists
	   and frequently visiting the new GNOME News & Discussion
	   site ({\tt http://news.gnome.org/gnome-news}).

	   If you encounter bugs or problems with GNOME components,
	   please submit a detailed report to the GNOME Bug Tracking
	   System using the {\tt gnome-bug} command (or its new
	   graphical version: {\tt bug-buddy} or use our bug tracking
	   system at {\tt http://bugs.gnome.org}.

     \item {\bf Technical Writers:} If you would like to help out with
	   the GNOME Documentation efforts you should check out the
	   Documentation projects page. {\tt http://www.gnome.org/gdp}

     \item {\bf Translators:} If you would like to help translate
	   GNOME components and GNOME documentation, you should check
	   out the GNOME Translation page. You can find it at:
	   {tt http://developer.gnome.org/arch/translate}

      \item {\bf Programmers:} Please check the GNOME Software map to
	   see or add programs already available for the GNOME
	   desktop. If you find some program of interest to you, you
	   might want to contact the author(s) of the code and offer
	   your help directly.

	   If you want to help out with the GNOME API Reference
	   Documentation, visit the Helping Out With GNOME API
	   Documentation page for instructions and the steps to take. 

	   If you want to work in GNOME, consider checking out the CVS
	   tree.

       \item {\bf Business Managers:} If your company or institution
	   can offer free software job openings for the GNOME desktop
	   and other GNU software projects, please submit job listings
	   to {\tt jobs@gnu.org}. You can read some technical
	   arguments for supporting such software in The Cathedral and
	   the Bazaar. This paper was behind Netscape's decision to
	   release its client software source code. The Free Software
	   Foundation ({\tt http://www.gnu.org}) maintains the Free
	   Software Job Page for listing opportunities for paid
	   employment.

\section{Thanks}

The GNOME project and the GNOME productivity applications would have
not existed without the help of the various free software enthusiasts,
technology enthusiasts, the various translation teams, the user
interface team, our web team and the users that have helped improve
the GNOME system since its begining.

Some of the most active hackers have been with us since the very
beginning, others are just joining us.  We would like to thank all of
you for making this happen.  Total Thanks!

\section{Authors}

George Lebl is an independent consultant in San Diego California.  He
has been involved in GNOME since the very early days.

Elliot Lee is a programmer at Red Had Advanced Development Labs at Red
Hat. 

Miguel de Icaza is a programmer at Helix Code, Inc.

\end{document}
